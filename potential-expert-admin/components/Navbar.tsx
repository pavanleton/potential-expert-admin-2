import { useRouter } from "next/router";
import { Flex, Box, Image, IconButton } from "@chakra-ui/react";
import { useContext } from "react";
import { AiOutlineHome } from "react-icons/ai";

// CONTEXT
import { UserContext } from "context/user";

const Navbar: React.FC = () => {
  const router = useRouter();

  const { user } = useContext(UserContext);

  return (
    <>
      <Flex
        justify={"space-between"}
        bg={"#121212"}
        px={"2rem"}
        py={"15px"}
        borderRadius={"10px"}
        alignItems={"center"}
      >
        <Box w={"100px"}>
          <Image
            src={"https://i.ibb.co/zX62Lff/NOVATIO-LOGO.png"}
            alt={"Novatio logo"}
            onClick={() => router.push("/orders")}
            cursor={"pointer"}
          />
        </Box>

        <Flex alignItems={'center'} gridGap={'10px'}> 
          <IconButton icon={<AiOutlineHome />} colorScheme={'drak'} aria-label={'Home'} onClick={() => router.push('/orders')} />
          <Box w={"50px"} height={"50px"}>
            <Image
              borderRadius="full"
              boxSize="150px"
              src={user.profile || "https://i.ibb.co/JRv6Vrc/Novatio-Icon.png"}
              alt="AVATAR"
              w={"100%"}
              h={"100%"}
              onClick={() => router.push("/profile")}
              cursor={"pointer"}
            />
          </Box>
        </Flex>
      </Flex>
    </>
  );
};

export default Navbar;
