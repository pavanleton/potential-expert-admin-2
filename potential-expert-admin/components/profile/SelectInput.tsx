import { Select } from "@chakra-ui/react";
import { useContext } from "react";

// CONTEXT
import { UserContext } from "context/user";

const SelectInput: React.FC<{ label: string; options: string[], id: string }> = ({
  label,
  options,
  id
}) => {

  const {updateUserProfile, user} = useContext(UserContext)  

  return (
    <>
      <Select
        placeholder={label}
        variant={"solid"}
        bg={"#3A3A3A"}
        _placeholder={{ color: "gray.500" }}
        id={id}
        // @ts-ignore
        value={user[id]}
        onChange={updateUserProfile}
      >
        {options.map((value: string, index: number) => (
          <option value={value} key={index}>
            {value}
          </option>
        ))}
      </Select>
    </>
  );
};

export default SelectInput;
