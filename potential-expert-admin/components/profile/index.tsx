import {
  Box,
  Flex,
  Image,
  Button,
  Grid,
  Container,
  Text,
} from "@chakra-ui/react";
import { useContext } from "react";

// CONTEXT
import { UserContext } from "context/user";

const ProfileItem: React.FC<{ label: string; value: string }> = ({
  label,
  value,
}) => {
  return (
    <>
      <Flex flexDir={"column"} alignItems={"flex-start"} gap={"6px"}>
        <Text
          textTransform={"uppercase"}
          fontSize={"sm"}
          fontWeight={"semibold"}
          color={"gray.500"}
        >
          {label}
        </Text>
        <Text fontSize={"lg"} fontWeight={"semibold"} color={"white"}>
          {value || "-"}
        </Text>
      </Flex>
    </>
  );
};

const ProfileCard: React.FC = () => {
  const { user } = useContext(UserContext);

  return (
    <>
      <Grid
        gridTemplateColumns={{ base: "1fr", md: "1fr 1fr" }}
        bg={"#121212"}
        p={"1rem"}
        w={"100%"}
        gap={"1rem"}
        borderRadius={"10px"}
        boxShadow={"lg"}
      >
        {Object.entries(user).map(
          (value: string[], index: number) =>
            !['id', 'profile'].includes(value[0]) && (
              <ProfileItem label={value[0]} value={value[1]} key={index} />
            )
        )}
      </Grid>
    </>
  );
};

export default ProfileCard;
