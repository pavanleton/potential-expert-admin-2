import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Grid,
  Box,
  Image,
  useToast
} from "@chakra-ui/react";
import { useState, useContext, useRef, useEffect, useMemo } from "react";
import { useUploadFile, useDownloadURL } from 'react-firebase-hooks/storage';
import { ref, getDownloadURL } from 'firebase/storage';
import { getFirestore, updateDoc } from 'firebase/firestore';

// COMPONENTS
import SelectInput from "./SelectInput";

// CONTEXT
import { UserContext } from "context/user";

// SERVICES
import { fileStorage, userCollection, fireStore} from "services/firebase";

// UTILS
import { industries, countryList } from "utils/data";

const EditProfile: React.FC<{ isOpen: boolean; onClose: VoidFunction }> = ({
  isOpen,
  onClose,
}) => {

  const [avatar, setAvatar] = useState({
    preview: '',
    fileObject: {name: ''}
  })

  const {user, updateUserProfile, updateProfileImage} = useContext(UserContext) 

  const UploadBtnRef = useRef<any>();
  const toast = useToast()

  useEffect(() => {

    if(!avatar.fileObject.name) {
        return
    }
    const objectUrl = URL.createObjectURL(avatar.fileObject as File)
    setAvatar({...avatar, preview: objectUrl})

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl)
  }, [avatar.fileObject])

  const [uploadFile, uploading, snapshot, error] = useUploadFile();

  const handleFileUpload = (event: any) => {
    event.preventDefault();
    const file = event?.target?.files.length && event?.target?.files[0];
    setAvatar({...avatar, fileObject: file})
    event.target.value = ''
  }

  const openFileSystem = (event: any) => {
    event.preventDefault();
    UploadBtnRef.current.click();
    return;
};

  const saveProfile = async () => {
    try {
        let userData = user;
        if(avatar.fileObject.name){
            const storageRef = ref(fileStorage, `/public/${avatar.fileObject.name}`)
            await uploadFile(storageRef, avatar.fileObject as any);
            const url = await getDownloadURL(storageRef)
            updateProfileImage(url)
            userData.profile = url;
        }        

        await updateDoc(userCollection(user.contact) , userData as any)      
        toast({
            title: 'Profile saved',
            duration: 3000,
            status: 'success',
            isClosable: true
        })
        
    } catch (error: any) {
        console.log(error);
    } 
  }

  console.log({
    snapshot, error
  });


  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} size={"5xl"} closeOnOverlayClick={false} >
        <ModalOverlay bg="blackAlpha.300" backdropFilter="blur(10px)" />
        <ModalContent bg={"black"} color={"white"}>
          <ModalHeader>Edit profile</ModalHeader>
          <ModalCloseButton />
          <ModalBody>

            <Flex flexDir={'column'} gap={'1rem'} >
                <FormControl>
                    <FormLabel htmlFor="email">Contact Information</FormLabel>
                    <Input variant={'solid'} bg={'#3A3A3A'} id="contact" type="email" placeholder="Email" value={user.contact} onChange={updateUserProfile} />
                </FormControl>

                <FormControl>
                    <FormLabel htmlFor="email">Upload Profile image</FormLabel>
                    <Flex border={'1px dashed'} borderColor={'rgba(255, 255, 255, 0.4)'} borderRadius={'10px'} p={'1rem'} alignItems={'center'} gap={'1rem'}>
                        <Box w={'50px'} h={'50px'} >
                            <Image
                                borderRadius="full"
                                src={user.profile || avatar.preview || "https://i.ibb.co/JRv6Vrc/Novatio-Icon.png"}
                                alt="AVATAR"
                                w={'100%'}
                                h={'100%'}
                                border={'2px solid'}
                                borderColor={'brand.500'}
                                objectFit={'cover'}
                            />
                           
                        </Box>
                        <Button size={'sm'} colorScheme={'brand'} onClick={openFileSystem}>Upload Image</Button>
                        <input type={'file'} style={{display: 'none'}} ref={UploadBtnRef} onChange={handleFileUpload} />
                    </Flex>
                </FormControl>

                <FormControl>
                <FormLabel htmlFor="contact-infp" fontWeight={600}>Bio</FormLabel>
                <Grid gridTemplateColumns={{base: '1fr', lg: 'repeat(3, 1fr)'}} gridGap={'1rem'}>
                    <Input variant={'solid'} bg={'#3A3A3A'} id="firstName" type="text" placeholder="First name" gridColumn={'span 2'}   value={user.firstName} onChange={updateUserProfile}/>
                    <Input variant={'solid'} bg={'#3A3A3A'} id="lastName" type="text" placeholder="Last name"  value={user.lastName} onChange={updateUserProfile}/>
                    <Box gridColumn={'span 3'} >
                        <SelectInput label="Gender" options={['male', 'female']} id={'gender'} />
                    </Box>
                    <Input variant={'solid'} bg={'#3A3A3A'} id="industryExperience" type="text" placeholder="Industry experience" gridColumn={'span 3'}  value={user.industryExperience} onChange={updateUserProfile}/>
                    <Input variant={'solid'} bg={'#3A3A3A'} id="linkedIn" type="text" placeholder="linkedIn profile link" gridColumn={'span 3'}  value={user.linkedIn} onChange={updateUserProfile}/>
                    <Input variant={'solid'} bg={'#3A3A3A'} id="role" type="text" placeholder="Role" gridColumn={'span 3'}  value={user.role} onChange={updateUserProfile} />
                    <Box gridColumn={{base: 'span 3', md: 'span 1'}}>
                        <SelectInput label="Years of Experience" options={["0-5", "5-10", "10+"]}  id={'yearsOfExperience'} />
                    </Box>
                    <Box gridColumn={{base: 'span 3', md: 'span 1'}}>
                        <SelectInput label="Domain" options={industries} id={'domain'} />
                    </Box>
                    <Box gridColumn={{base: 'span 3', md: 'span 1'}}>
                        <SelectInput label="Country" options={countryList} id={'country'} />
                    </Box>
                </Grid>
                </FormControl>
            </Flex>  
        
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="brand" variant="ghost" mr={3} onClick={onClose}>
              Close
            </Button>
            <Button colorScheme="brand" onClick={saveProfile} isLoading={uploading}>Save profile</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default EditProfile;
