import { Button } from "@chakra-ui/react";
import DataTable, { TableStyles, TableColumn } from "react-data-table-component";
import dayjs from 'dayjs'

interface ROW {
  id: number;
  name: string;
  serviceRequested: string;
  meetingscheduledTime: string | number;
  call: string;
  comments: number;
  action: any;
}

const Orders: React.FC = () => {
  const columns: TableColumn<ROW>[] = [
    {
      name: "Name",
      selector: (row: ROW) => row.name,
    },
    {
      name: "Service Requested",
      selector: (row: ROW) => row.serviceRequested,
    },
    {
      name: "Meeting scheduled time",
      selector: (row: ROW) => dayjs(row.meetingscheduledTime).format("h:mm A"),
    },
    {
      name: "Chat/Video call",
      selector: (row: ROW) => row.call,
    },
    {
      name: "Comments",
      selector: (row: ROW) => row.comments,
    },
    {
      name: "",
      selector: (row: ROW) => row.action,
    },
  ];

  const data: ROW[] = [
    {
      id: 1,
      name: "Nick Johnson",
      serviceRequested: "IT Services",
      meetingscheduledTime: Date.now(),
      call: "+58 87543 62832",
      comments: 228739,
      action: <Button colorScheme={"brand"} size={'sm'} px={'2rem'}>Join</Button>,
    },
    {
      id: 2,
      name: "William Nicholson",
      serviceRequested: "Finance",
      meetingscheduledTime: Date.now(),
      call: "+58 87543 62832",
      comments: 228739,
      action: <Button colorScheme={"brand"} size={'sm'} px={'2rem'}>Join</Button>,
    },
    {
      id: 3,
      name: "John wick",
      serviceRequested: "BPO",
      meetingscheduledTime: Date.now(),
      call: "+58 87543 62832",
      comments: 228739,
      action: <Button colorScheme={"brand"} size={'sm'} px={'2rem'}>Join</Button>,
    },
    {
      id: 4,
      name: "Agatha Christe",
      serviceRequested: "Sales and marketing",
      meetingscheduledTime: Date.now(),
      call: "+58 87543 62832",
      comments: 228739,
      action: <Button colorScheme={"brand"} size={'sm'} px={'2rem'}>Join</Button>,
    },
  ];

  const customStyles: TableStyles = {
    table: {
      style: {
        background: "transparent",
        color: "white",
      },
    },
    headRow: {
      style: {
        background: "#121212",
        color: "white",
        fontWeight: 700,
        fontSize: "16px",
        
      },
    },
    rows: {
      style: {
        background: "transparent",
        color: "white",
        minHeight: '72px',
      },
    },
    headCells: {
      style: {
        background: "transparent",
        padding: '1rem'
      },
    },
    cells: {
      style: {
        background: "transparent",
        color: "white",
        fontSize: "16px",
        padding: '1rem',
        borderBottom: '1px solid #232323 '
      },
    },
    pagination: {
      style: {
        background: "transparent",
        color: "white",
      },
    },
  };

  return (
    <DataTable
      columns={columns}
      data={data}
      pagination
      fixedHeader
      customStyles={customStyles}
    />
  );
};

export default Orders;
