import { extendTheme } from "@chakra-ui/react";
import { theme as chakraTheme } from "@chakra-ui/react";

import { ColorPallete } from "./colors";

const OverrideTheme = {
  ...chakraTheme,
  ...ColorPallete,
  styles: {
    ...chakraTheme.styles,
    global: {
      ...chakraTheme.styles.global,
      body: {
        color: "#FFFFFF",
        bg: "#000",
        fontFamily: "Cabin",
      },
    },

  },
};

const NavotioTheme = extendTheme(OverrideTheme);
export default NavotioTheme;
