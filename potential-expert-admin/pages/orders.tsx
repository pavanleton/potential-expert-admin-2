import { NextPage } from "next";
import Head from "next/head";
import React from "react";
import { Box, Heading } from "@chakra-ui/react";

// COMPONENTS
import OrdersDataTable from "components/orders";

// LAYOUT
import DefaultLayout from "layout/default";

// TYPES
import PageWithLayoutType from "types/pageWithLayout";

const Orders: NextPage = () => {
  

  return (
    <>
      <Head>
        <title>Orders</title>
      </Head>

      <Box my={'2rem'}>
        <Heading my={'1rem'} color={'white'} as={'h3'} size={'lg'} >My Matches</Heading>  
        <OrdersDataTable />
      </Box>
    </>
  );
};

(Orders as PageWithLayoutType).layout = DefaultLayout;

export default Orders;
