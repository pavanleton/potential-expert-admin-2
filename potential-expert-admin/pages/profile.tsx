import { NextPage } from "next";
import Head from "next/head";
import React, { useContext } from "react";
import {
  Box,
  Flex,
  Image,
  Button,
  Container,
  useDisclosure,
} from "@chakra-ui/react";

// COMPONENTS
import ProfileCard from "components/profile";
import EditProfile from "components/profile/EditProfile";

// LAYOUT
import DefaultLayout from "layout/default";

// TYPES
import PageWithLayoutType from "types/pageWithLayout";

// CONTEXT
import { UserContext } from "context/user";

const Profile: NextPage = () => {
  const {
    isOpen: isEditProfileOpen,
    onOpen: openEditProfile,
    onClose: closeEditProfile,
  } = useDisclosure();

  const { user } = useContext(UserContext);

  return (
    <>
      <Head>
        <title>Profile</title>
      </Head>
      <EditProfile isOpen={isEditProfileOpen} onClose={closeEditProfile} />

      <Flex
        my={"2rem"}
        flexDir={"column"}
        alignItems={"center"}
        width={"100%"}
        height={"100vh"}
        gap={"1.5rem"}
      >
        <Flex flexDir={"column"} gap={"1.5rem"} alignItems={"center"}>
          <Box
            w={{ base: "150px", md: "200px" }}
            height={{ base: "150px", md: "200px" }}
          >
            <Image
              borderRadius="full"
              boxSize="150px"
              border={"3px solid"}
              borderColor={"brand.600"}
              src={user.profile || "https://i.ibb.co/JRv6Vrc/Novatio-Icon.png"}
              alt="Avatar"
              w={"100%"}
              h={"100%"}
            />
          </Box>
          <Box>
            <Button colorScheme={"brand"} onClick={openEditProfile}>
              Edit Profile
            </Button>
          </Box>
        </Flex>

        <Container maxW={"container.md"}>
          <ProfileCard />
        </Container>
      </Flex>
    </>
  );
};

(Profile as PageWithLayoutType).layout = DefaultLayout;

export default Profile;
