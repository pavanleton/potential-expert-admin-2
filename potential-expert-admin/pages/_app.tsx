import { ChakraProvider } from "@chakra-ui/react";
import Head from "next/head";
import React from "react";

// THEME
import NovatioTheme from "theme";

// CONTEXT
import UserProvider from "context/user";

// TYPES
import { AppLayoutProps } from "types/_app";

// STYLES
import "../styles/global.module.scss";

function MyApp({ Component, pageProps }: AppLayoutProps) {
  const Layout = Component.layout || (({ children }: any) => <>{children}</>);

  return (
    <>
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin={"true"}
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Cabin:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap"
          rel="stylesheet"
        />
      </Head>

      <ChakraProvider theme={NovatioTheme}>
        <UserProvider>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </UserProvider>
      </ChakraProvider>
    </>
  );
}

export default MyApp;
