import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  Checkbox,
  Stack,
  Link,
  Button,
  Heading,
  Text,
  useColorModeValue,
  useToast,
} from "@chakra-ui/react";
import { useEffect, useState, useContext } from "react";
import { useRouter } from "next/router";
import {
  useAuthState,
  useCreateUserWithEmailAndPassword,
  useSignInWithEmailAndPassword,
} from "react-firebase-hooks/auth";
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
} from "firebase/auth";
import { getDoc, setDoc } from "firebase/firestore";

// SERVICES
import { firebase, userCollection } from "services/firebase";

// CONTEXT
import { UserContext } from "context/user";

const config = {
  login: {
    mainText: "Sign in to your account",
    redirectText: "Don't have an account?",
    buttonText: "Sign In",
  },
  register: {
    mainText: "Create an account",
    redirectText: "Already have an account?",
    buttonText: "Create",
  },
};

export default function SimpleCard() {
  const [authMode, setAuthMode] = useState<string>("login");
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [serverError, setServerError] = useState<string>("");
  const router = useRouter();

  const fbAuth = getAuth(firebase);

  const [register, newUser, registering, registrationError] =
    useCreateUserWithEmailAndPassword(fbAuth);

  const [login, user, loggingin, loginError] =
    useSignInWithEmailAndPassword(fbAuth);

  const toast = useToast();

  const { updateUser } = useContext(UserContext);

  useEffect(() => {
    setServerError("");
  }, [email, password, authMode]);

  useEffect(() => {
    if (loginError) {
      toast({
        title: loginError?.code.replace("auth/", "").split("-").join(" "),
        duration: 3000,
        status: "error",
        isClosable: true,
      });
      return;
    }

    if (registrationError) {
      setServerError("Please enter valid data to register!");
      toast({
        title: registrationError?.code
          .replace("auth/", "")
          .split("-")
          .join(" "),
        duration: 3000,
        status: "error",
        isClosable: true,
      });
      return;
    }
  }, [registrationError, loginError]);

  const toggleMode = () =>
    authMode === "login" ? setAuthMode("register") : setAuthMode("login");

  const handleSubmit = async () => {
    try {
      let res;
      setServerError("");

      if (!email) {
        toast({
          title: "Please enter email",
          duration: 3000,
          status: "error",
          isClosable: true,
        });

        return;
      }

      if (!password) {
        toast({
          title: "Please enter password",
          duration: 3000,
          status: "error",
          isClosable: true,
        });

        return;
      }

      if (authMode === "login") {
        const dd = await signInWithEmailAndPassword(fbAuth, email, password);
        console.log({ dd });

        const data = await getDoc(userCollection(email));
        updateUser({ ...data.data() });

        router.push("/orders");
      } else {
        const dd = await createUserWithEmailAndPassword(
          fbAuth,
          email,
          password
        );
        console.log({ dd });

        updateUser({ contact: email });

        await setDoc(userCollection(email), {
          id: newUser?.user?.uid || `${Date.now()}`,
          contact: email,
        });

        router.push("/profile");
      }
    } catch (e: any) {
        console.log({e});
        
      toast({
        title: e?.code
          .replace("auth/", "")
          .split("-")
          .join(" "),
        duration: 3000,
        status: "error",
        isClosable: true,
      });
      // @ts-ignore
    //   setServerError(e.response?.data?.error?.message);
    }
  };

  return (
    <Flex
      minH={"100vh"}
      align={"center"}
      justify={"center"}
      bg={useColorModeValue("black.50", "white.800")}
    >
      <Stack spacing={8} mx={"auto"} maxW={"lg"} py={12} px={6}>
        {/* eslint-disable-next-line @next/next/no-img-element */}
        <img
          width={"100%"}
          src={"https://i.ibb.co/DfkckKP/NOVATIO.png"}
          alt={"Novatio logo"}
        />
        <Stack align={"center"}>
          {/*// @ts-ignore*/}
          <Heading fontSize={"4xl"}>{config[authMode]["mainText"]}</Heading>
        </Stack>
        <Box rounded={"lg"} bg={"#121212"} boxShadow={"lg"} p={8}>
          <Stack spacing={4}>
            <FormControl id="email">
              <FormLabel>Email address</FormLabel>
              <Input
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormControl>
            <FormControl id="password">
              <FormLabel>Password</FormLabel>
              <Input
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormControl>
            <Stack spacing={10}>
              <Stack
                direction={{ base: "column", sm: "row" }}
                align={"start"}
                justify={"space-between"}
              >
                <Link color={"blue.400"} onClick={toggleMode}>
                  {/*// @ts-ignore*/}
                  {config[authMode]["redirectText"]}
                </Link>
                {/*<Link color={'blue.400'}>Forgot password?</Link>*/}
              </Stack>

              <Button
                colorScheme={"brand"}
                onClick={handleSubmit}
                isLoading={registering || loggingin}
              >
                {/*// @ts-ignore*/}
                {config[authMode]["buttonText"]}
              </Button>
            </Stack>
          </Stack>
        </Box>
      </Stack>
    </Flex>
  );
}
