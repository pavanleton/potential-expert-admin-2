import type { NextPage } from "next";
import React from "react";
import { Box, Flex, Container, Image, Button, Grid, Text } from "@chakra-ui/react";
import { useRouter } from "next/router";

// ASSETS
import Logo from "asset/svg/Logo";

const Splash: NextPage = () => {
  const router = useRouter();
  return (
      <Container py={"2rem"} maxW={"container.xl"} h={"100vh"}>
        <Box d={{base: 'none', lg: 'block'}}>
          <Logo />
        </Box>

        <Grid height={"100vh"} gridTemplateColumns={{ base: "1fr", lg: "1.5fr 1fr" }} alignItems={'center'} justifyContent={'center'} >
          <Box d={{base: 'none', lg: 'block'}}>
            <Image
                src={"https://i.ibb.co/RgBxvLK/splash-scrren-logo.png"}
                alt={"Logo with Subtitle"}
                w={"100%"}
            />
          </Box>

          <Flex
              height={"100vh"}
              justify={"center"}
              alignItems={"center"}
              flexDirection={"column"}
              gap={"20px"}
              w={'100%'}
          >
            <Box w={{base: "80%", md: '400px', lg: '200px'}}>
              <Image
                  src={"https://i.ibb.co/4M61V6L/logo-with-sub.png"}
                  alt={"Logo with Subtitle"}
                  w={"100%"}
              />
            </Box>
            <Button
                onClick={() => router.push('/auth')}
                colorScheme={"brand"}
                size={"lg"}>
              Login
            </Button>
          </Flex>
        </Grid>
      </Container>
  );
};

export default Splash;
