import React, { useState, useEffect } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { getAuth } from "firebase/auth";
import { doc, getDoc } from "firebase/firestore";

// SERVICES
import { firebase, userCollection } from "services/firebase";

// TYPES
import { USER } from "types/_app";

interface UserContextParams {
  user: USER;
  updateUserProfile: (event: any) => void;
  updateProfileImage: (url: string) => void;
  reset: () => void;
  updateUser: (data: {[key: string]: string}) => void
}

export const UserContext = React.createContext<UserContextParams>({
  user: {
    firstName: "",
    lastName: "",
    contact: "",
    gender: "",
    industryExperience: "",
    linkedIn: "",
    role: "",
    yearsOfExperience: "",
    domain: "",
    country: "",
    profile: "",
    id: "",
  },
  updateUserProfile: (event: any) => {},
  updateProfileImage: (url: string) => {},
  reset: () => {},
  updateUser: (data: {[key: string]: string}) => {}
});

const UserProvider: React.FC = ({ children }) => {
  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    contact: "",
    gender: "",
    industryExperience: "",
    linkedIn: "",
    role: "",
    yearsOfExperience: "",
    domain: "",
    country: "",
    profile: "",
    id: "",
  });

  const fbAuth = getAuth(firebase);

  const [userFromFb, loading, error] = useAuthState(fbAuth);

  const updateUserProfile = (event: any) => {
    setUser({ ...user, [event.target.id]: event.target.value });
  };
  const updateUser = (data: {[key: string]: string}) => {
      setUser({...user, ...data})
  }

  useEffect(() => {
    if (userFromFb) {
      getDoc(userCollection(userFromFb?.email as string)).then((data) =>
        setUser({ ...user, ...data.data() })
      );
    }
  }, [userFromFb]);

  const updateProfileImage = (url: string) => {
    setUser({ ...user, profile: url });
  };

  const reset = () => {
    setUser({
      firstName: "",
      lastName: "",
      contact: "",
      gender: "",
      industryExperience: "",
      linkedIn: "",
      role: "",
      yearsOfExperience: "",
      domain: "",
      country: "",
      profile: "",
      id: "",
    });
  };

  return (
    <UserContext.Provider
      value={{ user, updateUserProfile, updateProfileImage, reset, updateUser }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserProvider;
