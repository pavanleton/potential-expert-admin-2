import { ReactChild } from "react";
import { Flex, Container } from "@chakra-ui/react";

// COMPONENTS
import Navbar from "components/Navbar";

const DefaultLayout: React.FC<{ children: ReactChild }> = ({children}) => {
    return <>
        <Container py={'1rem'} maxW={'container.xl'}>
           <Navbar />    
           {children}
        </Container>
    </>
}

export default DefaultLayout;