import { NextPage } from 'next'

// LAYOUT
import ServicesLayout from 'layout/default';

type PageWithIndustriesLayoutType = NextPage & { layout: typeof ServicesLayout }

type PageWithLayoutType = PageWithIndustriesLayoutType 

export default PageWithLayoutType
