import PageWithLayoutType from "./pageWithLayout";

type AppLayoutProps = {
  Component: PageWithLayoutType;
  pageProps: any;
};



interface USER {
  firstName: string;
  lastName: string;
  contact: string;
  gender: string ;
  industryExperience: string | number;
  linkedIn: string;
  role: string;
  yearsOfExperience: string | number;
  domain: string;
  country: string;
  profile: string;
  id: string
}

export { AppLayoutProps, USER };
