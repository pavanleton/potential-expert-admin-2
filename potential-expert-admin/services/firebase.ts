import { initializeApp } from "firebase/app";
import { getFirestore, doc } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: "AIzaSyAWQ-b8j54hZSWJvzQIFy7igXgiivhquPs",
  authDomain: "navotia.firebaseapp.com",
  projectId: "navotia",
  storageBucket: "navotia.appspot.com",
  messagingSenderId: "996189991512",
  appId: "1:996189991512:web:c26e419a2f0e853db8502d"
};

export const firebase = initializeApp(firebaseConfig);
export const fireStore = getFirestore(firebase)
export const userCollection = (path: string) => doc(fireStore, 'users', path)
export const fileStorage = getStorage(firebase);
