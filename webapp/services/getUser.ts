export const getUser = () => {
    const userInStore = localStorage.getItem('novatioUser');
    if(!userInStore) {
        return {status: 'Not Found!', userExist: false}
    }

    const user = JSON.parse(userInStore);

    return {userExist: true, ...user}
}