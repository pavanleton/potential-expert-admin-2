import { getAPIInstance } from "services/api/config";
import qs from "qs";

// TYPES
import { SubProcessType, ProcessType } from "context/process";

export const getSuperTypes = () =>
  getAPIInstance({ tokenType: 'cms' }).get("/super-types", {
    params: {
      populate: "coverImage",
    },
  });

export const getTypesBySuper = (superType: string | number) =>
  getAPIInstance({ tokenType: 'cms' }).get(
    `/super-types/${superType}?${qs.stringify({
      populate: {
        types: {
          populate: ["svgIcon"],
        },
      },
    })}`
  );

export const getCategories = (types: string | number) =>
  getAPIInstance({ tokenType: 'cms' }).get(
    `/types/${types}?${qs.stringify({
      populate: ["categories"],
    })}`
  );

export const getProducts = (categoryId: string | number) =>
  getAPIInstance({ tokenType: 'cms' }).get(
    `/categories/${categoryId}?${qs.stringify({
      populate: {
        products: {
          populate: ["coverImage"],
        },
      },
    })}`
  );

export const getProduct = (productId: string | number) =>
  getAPIInstance({ tokenType: 'cms' }).get(
    `/products/${productId}?${qs.stringify({
      populate: ["processLevels"],
    })}`
  );

export const getParentProcess = (productId: string | number, userId?: number | string) =>
  getAPIInstance({ tokenType: userId ? 'admin': 'user' }).get(
    `/product-process-trackers/me/?product=${productId}&userId=${userId}`
  );

export const getSubProcess = (processId: string | number, userId?: number | string) =>
    getAPIInstance({ tokenType: userId ? 'admin': 'user' }).get(
    `/process-level-trackers/me/?processLevel=${processId}&userId=${userId}`
  );

export const updateSubProcess = (
  processLevelId: string | number,
  subProcesses: SubProcessType[],
  userId?: string,
) =>
  getAPIInstance({ tokenType: userId ? 'admin': 'user' }).patch(`/process-level-trackers/me`, {
      userId: userId,
      data: {
      processLevelId,
      trackingInfo: subProcesses.map((sp: SubProcessType) => ({
        id: sp.id,
        name: sp.title,
        description: sp.content,
        isCompleted: sp.isCompleted,
      })),
    },
  });

export const updateProcess = (
  productId: string | number,
  processes: ProcessType[],
  userId?: string,
) =>
  getAPIInstance({ tokenType: userId ? 'admin': 'user' }).patch(`/product-process-trackers/me`, {
      userId: userId,
      data: {
      productId,
      trackingInfo: processes.map((p: ProcessType) => ({
        id: p.id,
        name: p.title,
        isCompleted: p.isCompleted,
      })),
    },
  });

export const getSKUProducts = () =>
    getAPIInstance({ tokenType: 'none' }).get('/products/sku')
