import { getAPIInstance } from "services/api/config";

export const createOrder = (shipping: any, products: any[], SKUs: string[]) => {
  const data = {
    orderDetails: {
      ...shipping,
      name: `${shipping.fname} ${shipping.lname}`,
      zipCode: shipping.zip,
    },
    products,
    SKUs,
  };
  if (SKUs.length) {
    // @ts-ignore
    delete data.products
    return getAPIInstance({ tokenType: 'user' }).post('/orders/place/direct', data);
  }

  return getAPIInstance({ tokenType: 'user' }).post('/orders/place', data);
};


export const verifyPayment = (orders: string[]) =>
    getAPIInstance({ tokenType: 'user' }).patch('/orders/payment-status',{ orders });

export const getUserOrders = () =>
    getAPIInstance({ tokenType: 'user' }).get('/orders/me');