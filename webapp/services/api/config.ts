import axios from "axios";
const getToken = (tokenType: string) => {
    if (tokenType === 'user')
        if (localStorage && localStorage.novatioUser)
            return JSON.parse(localStorage.novatioUser).token
    if(tokenType === 'cms')
        return process.env.NEXT_PUBLIC_CMS_PUBLIC_TOKEN;

    if (tokenType === 'admin')
        if (localStorage && localStorage.novatioAdminToken)
            return localStorage.novatioAdminToken

    return ''
}
export const getAPIInstance = ({ tokenType }: { tokenType: 'none' | 'user' | 'cms' | 'admin' }) => {
    const config: { baseURL: string | undefined, headers?: { Authorization: string } } = {
        baseURL: process.env.NEXT_PUBLIC_CMS_API,
    };
    if (tokenType !== 'none')
        config.headers = {
            'Authorization': `Bearer ${getToken(tokenType)}`,
        }
    return axios.create(config);
};
// 9999899998
export const register =
    ({ email, password, username }: { email: string; password: string; username: string; }) =>
        getAPIInstance({ tokenType: "none" }).post('/auth/local/register', {
            email,
            password,
            username,
});

export const login =
    ({ identifier, password }: { identifier: string; password: string; }) =>
        getAPIInstance({ tokenType: "none" }).post('/auth/local', {
            identifier,
            password,
});

