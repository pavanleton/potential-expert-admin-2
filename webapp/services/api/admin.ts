import { getAPIInstance } from "services/api/config";

export const getOrders = () =>
    getAPIInstance({ tokenType: 'cms' }).get('/orders');

export const login = (email: string, password: string) =>
    getAPIInstance({ tokenType: 'none' }).post('/user/admin/login', { email, password })