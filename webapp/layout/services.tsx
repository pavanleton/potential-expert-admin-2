import React, { ReactChild, useState } from "react";
import { Box, Flex, Image, IconButton } from "@chakra-ui/react";
import { useRouter } from "next/router";
import {HiOutlineChevronDoubleLeft} from 'react-icons/hi'

// COMPONENTS
import Breadcrumbs from "components/Breadcrumbs";
import SubMenuSideBar from "components/sidebars/SubMenu";
import MenuIcons from "components/sidebars/MenuIcons";

// SERVICES
import { getCategories } from "services/api/store";

const ServicesLayout: React.FC<{ children: ReactChild }> = ({ children }) => {
  const [categories, setCategories] = useState([]);
  const [expandMainMenu, setExpandMainMenu] = useState(false);

  const router = useRouter();
  const categoryId = router.query?.catId || 1;

  React.useEffect(() => {
    getCategories(categoryId as string | number).then(res => { console.log({res});
     setCategories(res?.data?.data?.attributes?.categories?.data || [])})
  }, [categoryId, router.pathname])

  const closeSideBar = () => {
    setExpandMainMenu(false)
  }

  const closeSidebarWithDelay = () => {
    let timeout: any;

    const cb = () => {
        closeSideBar();
    };

    clearTimeout(timeout);

    timeout = setTimeout(cb, 500);
  };


  return (
    <>
      <Flex w={'100%'}  gap={'.4rem'} pos={'relative'} h={'100vh'}
      flexDir={{base: 'column', lg:'row'}}
      css={{
        '&::-webkit-scrollbar':{
          display: 'none'
        }
      }}
      >
        <Flex d={{base: 'none', lg: 'flex'}} flexDir={'column'} gap={'1rem'} w={expandMainMenu ? '22%' : '70px'} transition={'all .3s linear'} bg={'#121212'} alignItems={'flex-start'} py={'1rem'}>
          <Flex justifyContent={'space-between'} w={'100%'} alignSelf={'center'} px={'0.5rem'}>
                  <Box w={"50px"} h={"50px"}>
                    <Image
                      src={"https://i.ibb.co/JRv6Vrc/Novatio-Icon.png"}
                      alt={"Novatio logo"}
                      w={"100%"}
                      h={"100%"}
                      cursor={"pointer"}
                      onClick={() => router.push("/home")}
                    />
                  </Box>
                  { expandMainMenu && <IconButton colorScheme={'dark'} icon={<HiOutlineChevronDoubleLeft />} aria-label={'Close sidebar'} onClick={closeSideBar} />}
          </Flex>
          <Flex flexDir={"column"} alignItems={"center"} gap={".4rem"} w={'fit-content'} onMouseLeave={closeSidebarWithDelay}>
                  <MenuIcons list={[]} isMenuVisible={expandMainMenu} toggleMainMenu={setExpandMainMenu} />
          </Flex>
        </Flex>
        <Flex width={'100%'} gap={'1rem'} overflowX={'scroll'} overflowY={'scroll'}
        zIndex={100}
        flexDir={{base: 'column', lg:'row'}}
        css={{
            '&::-webkit-scrollbar':{
              display: 'none'
            }
        }}
        >
            <Box   w={{base: '100%', lg: '30%'}}>
              <SubMenuSideBar list={categories} />
            </Box>
            <Flex py={"1rem"}   width={'100%'} flexDirection={"column"} height={'100vh'} overflowY={'scroll'} overflowX={'hidden'}  
            css={{
                '&::-webkit-scrollbar':{
                  display: 'none'
                }
            }}
              >
              <Breadcrumbs sku={false} />
              {children}
            </Flex>
        </Flex>

      </Flex>

     
    </>
  );
};

export default ServicesLayout;
