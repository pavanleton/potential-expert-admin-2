import { NextPage } from 'next'

// LAYOUT
import ServicesLayout from 'layout/services';

type PageWithIndustriesLayoutType = NextPage & { layout: typeof ServicesLayout }

type PageWithLayoutType = PageWithIndustriesLayoutType 

export default PageWithLayoutType
