import React, { ReactChild, useState } from "react";
import { useRouter } from "next/router";
import { position, useBoolean, useToast } from '@chakra-ui/react'

// SERVICES
import {
  getSubProcess,
  getParentProcess,
  updateSubProcess,
  updateProcess,
} from "services/api/store";

export type ProcessType = {
  id: number;
  title: string;
  isCompleted?: boolean;
};

export type SubProcessType = {
  id: number;
  title: string;
  pid: number;
  isCompleted: boolean;
  content: string;
};

interface ProcessContextTypes {
  process: ProcessType[];
  subProcess: SubProcessType[];
  selectedProcess: number;
  updateSelection: (id: number) => void;
  toggleSubProcess: (spId: number) => void;
  toggleProcess: (status: boolean) => void;
  saveSubProcess: () => void;
  triggerRefetch: () => void;
  updateSubProcessContent: (spId: number, value: string) => void;
  setIsUnsaved: {
    on: () => void;
    off: () => void;
    toggle: () => void;
  }
}

export const ProcessContext = React.createContext<ProcessContextTypes>({
  process: [],
  subProcess: [],
  selectedProcess: 1,
  updateSelection: (id: number) => {},
  toggleSubProcess: (spId: number) => {},
  toggleProcess: (status: boolean) => {},
  saveSubProcess: async () => {},
  triggerRefetch: () => {},
  updateSubProcessContent: (spId: number, value: string) => {},
  setIsUnsaved: {
    on: () => {},
    off: () => {},
    toggle: () => {},
  }
});

export const ProcessProvider: React.FC<{ children: ReactChild }> = ({
  children,
}) => {
  const [process, setProcess] = useState<ProcessType[]>([]);

  const [subProcess, setSubProcess] = useState<SubProcessType[]>([]);
  const [selectedProcess, setSelectedProcess] = useState(1);
  const [isAdmin, setIsAdmin] = useState(false);
  const [rand, setRand] = useState(true)
  const [userId, setUserId] = useState('');


  const [isUnsaved, setIsUnsaved] = useBoolean()

  const triggerRefetch = () => setRand(!rand)

  const router = useRouter();
  const toast = useToast();

  const prodId = router.query.name || 1;

  React.useEffect(() => {
    // @ts-ignore
    setIsAdmin(router.query.adminView === true);
    // @ts-ignore
    setUserId(router.query.userId)
  }, [router.query]);
  
    React.useEffect(() => {
      if (router.pathname.includes("/details")) {
        setTimeout(() => {
          getProcesses();
        }, 2000)
      }
    }, [prodId, rand]);
  
    React.useEffect(() => {
      if (router.pathname.includes("/details")) {
        getSubProcess(selectedProcess as string | number, userId as string)
          .then((resSubPro) => {
            const state = resSubPro?.data?.data?.trackingInfo.map((pcs: any) => ({
              id: pcs.id,
              title: pcs.name,
              content: pcs.description,
              pid: selectedProcess,
              isCompleted: pcs.isCompleted,
            }));
  
            setSubProcess(state);
          })
          .catch((err) => {throw err});
      }
    }, [selectedProcess]);

  const generateErrorToast = (content: {title: string, description?: string}) => {
    return toast({
      ...content,
      status: 'error',
      duration: 3000,
      isClosable: true
    })
  }
     
  const getProcesses = () => {
    getParentProcess(prodId as string | number, userId as string)
        .then((resPro) => {
          const state = resPro?.data?.data?.trackingInfo?.map((pcs: any) => ({
            id: pcs.id,
            title: pcs?.name,
            isCompleted: pcs?.isCompleted || false,
          }));
          setProcess(state || []);
        })
        .catch((err) => generateErrorToast({title: err?.message || 'Unable to retrive process, please try again'}));
  }

  const updateSelection = (id: number) => {
    
    if(isUnsaved) {
      generateErrorToast({title: 'Please save the current changes, before proceeding further.'})
      return
    }
    setSelectedProcess(id);
  };

  const toggleSubProcess = async (spId: number) => {
    try {
      const state = subProcess.map((pcs: SubProcessType) => {
        if (pcs.id === spId) {
          pcs.isCompleted = !pcs.isCompleted;
          return pcs;
        }
  
        return pcs;
      });
  
      setSubProcess(state);
      // @ts-ignore
      await updateSubProcess(selectedProcess, state, userId);

    } catch (error: any) {
        generateErrorToast({title: error?.message || 'Unable to update subprocess, please try again'})
        return
    }
  };

  const toggleProcess = async (status: boolean) => {
    try {
      
      const state = process.map((pcs: ProcessType) => {
        if (pcs.id === selectedProcess) {
          pcs.isCompleted = status;
          return pcs;
        }

        return pcs;
      });

      setProcess(state);
      // @ts-ignore
      await updateProcess(parseInt(`${prodId}`), state, userId);
    } catch (error: any) {
      generateErrorToast({title: error?.message || 'Unable to update process, please try again'})
      return
    }
  };

  const saveSubProcess = async () => {
    try {
      toast({
        title: 'Saving changes.....',
        id: 'SP_SAVING_TOAST',
        status: 'info',
        isClosable: false,
        position: 'top-right'
      })
      const state = subProcess.filter(
        (pcs: SubProcessType) => pcs.pid === selectedProcess && pcs
      );

      await updateSubProcess(parseInt(`${selectedProcess}`), state);
      setIsUnsaved.off();
      toast.close('SP_SAVING_TOAST')
      toast({
        title: 'Changes Saved ✅',
        status: 'success',
        duration: 2000,
        isClosable: true,
        position: 'top-right'
      })
    } catch (error: any) {
      toast.close('SP_SAVING_TOAST')
      generateErrorToast({title: error?.message || 'Unable to save subprocess, please try again'})
      return
    }
  };

  const updateSubProcessContent = (spId: number, value: string) => {

    const state = subProcess.map((pcs: SubProcessType) => {
      if (pcs.id === spId) {            
        pcs.content = value;
        return pcs;
      }

      return pcs;
    });    

    setSubProcess(state);
    
  }
  console.log({isUnsaved});

  return (
    <ProcessContext.Provider
      value={{
        process,
        subProcess,
        updateSelection,
        selectedProcess,
        toggleSubProcess,
        saveSubProcess,
        toggleProcess,
        triggerRefetch,
        updateSubProcessContent,
        setIsUnsaved
      }}
    >
      {children}
    </ProcessContext.Provider>
  );
};
