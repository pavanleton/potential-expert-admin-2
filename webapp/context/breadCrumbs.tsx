import React, { ReactChild, useState } from "react";
import { useRouter } from "next/router";

// SERVICES
import {getProducts, getTypesBySuper, getCategories, getProduct} from "services/api/store"

export type Levels = {
    st: number, t: number, cat: number, prod: number, pdid: number
}

export type LevelsEnum = 'st' | 't' | 'cat' | 'prod' | 'pdid'

interface BreadcrumbsContextTypes {
    levels: Levels,
    clearAllLevels: VoidFunction, 
    clearLevel: (levelType: LevelsEnum) => void, 
    addLevel: (levelType: LevelsEnum, id: number) => void 
    generateBreadCrumbs: () => Promise<string[]>
    handleClick: (lvl: LevelsEnum) => void
}

const defaultLevel = {st: 1, t: 1, cat: 1, prod: 2, pdid: 1}

export const BreadcrumbsContext = React.createContext<BreadcrumbsContextTypes>({
    levels: defaultLevel,
    clearAllLevels: () => {}, 
    clearLevel: (levelType: LevelsEnum) => {}, 
    addLevel: (levelType: LevelsEnum, id: number) => {},
    generateBreadCrumbs: async () => [],
    handleClick: (lvl: LevelsEnum) => {}
})

export const BreadcrumbsProvider: React.FC<{children: ReactChild}> = ({children}) => {

    const [levels, setLevels] = useState<Levels>(defaultLevel);

    const router = useRouter()

    const clearAllLevels = () => {
        setLevels(defaultLevel)
    }

    const clearLevel = (levelType: LevelsEnum) => {
        const state = {...levels, [levelType]: 1}
        setLevels(state)
    }

    const addLevel = (levelType: LevelsEnum, id: number) => {
        const state = {...levels, [levelType]: id}
        setLevels(state)
    }

    const generateBreadCrumbs = async (): Promise<string[]> => {
       try{
            const superType = await getTypesBySuper(levels.st)
            const type = await getCategories(levels.t)
            const category = await getProducts(levels.pdid)
            
            if(router.pathname.includes('/details')){
                const product = await getProduct(levels.prod)
            
                return [superType, type, category, product].map(({data}: any) => data.data.attributes.name)
            }

            return [superType, type, category].map(({data}: any) => data.data.attributes.name)
       }catch(err: any){
           console.log(err);
           return [];
       }
    }

    const handleClick = (level: LevelsEnum) => {
        const path = {
            st: '/home',
            t: `/product-types?superType=${levels.st}`, 
            cat: `/services?catId=${levels.cat}&pdId=${levels.pdid}`, 
            prod: 'details/',
            pdid: ''
        }

        if(level === 'prod'){
            router.push(`${path[level]}${levels[level]}/?catId=${levels.cat}&pdId=${levels.prod}`)
            return
        }

        router.push(`${path[level]}`)
    }
    

    return <BreadcrumbsContext.Provider value={{levels, clearAllLevels, clearLevel, addLevel, generateBreadCrumbs, handleClick}}>
        {children}
    </BreadcrumbsContext.Provider>
}
