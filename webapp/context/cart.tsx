import React, { ReactChild, useState } from "react";

interface Cost {
    marketplace: number;
    productsAndServices: number;
}
interface CartTypes {
    services: any[],
    addToCart: (product: any) => void,
    removeFromCart: (id: string) => void,
    emptyCart: () => void,
    ttlCost: Cost,
    setCart: (state: any[]) => void;
}

export const CartContext = React.createContext<CartTypes>({
    services: [],
    addToCart: (product: any) => {},
    removeFromCart: (id: string) => {},
    emptyCart: () => {},
    ttlCost: { marketplace: 0, productsAndServices: 0 },
    setCart: (state: any[]) => {},
})

export const CartProvider: React.FC<{children: ReactChild}>  = ({children}) => {
    const [services, setServices] = useState<any[]>([]);
    const [ttlCost, setTtlcost] = useState<number>(0);

    React.useEffect(() => {
        if(localStorage && localStorage.getItem('navotia_cart')){
            const state = JSON.parse(localStorage.getItem('navotia_cart') as string)
            setServices(state.cart)
        }
    }, [])

    React.useEffect(() => {
        if (services.length)
            setTtlcost(services.length > 0 ? services.reduce((prevVal, curVal,) => {
                let orderType = curVal.isDirectOrder ? 'productsAndServices' : 'marketplace';
                prevVal[orderType] = prevVal[orderType] + Number(curVal.price || curVal.attributes?.price)
                return prevVal;
            }, {
                marketplace: 0,
                productsAndServices: 0
            }) : {
                marketplace: 0,
                productsAndServices: 0
            });
        
    }, [services])

    const addToCart = (product: any) => {
        const state = [...services, product]
        setServices(state)
        console.log({ state })
        localStorage.setItem('navotia_cart', JSON.stringify({cart: state}))
    }

    const setCart = (state: any) => {
        setServices(state)
        localStorage.setItem('navotia_cart', JSON.stringify({cart: state}))
    }

    const removeFromCart = (id: string) => {
        const state = services.filter((service: any) => `${service.id}` !== id)
        setServices(state);
        localStorage.setItem('navotia_cart', JSON.stringify({cart: state}))
    }

    const emptyCart = () => {
        setServices([])
        localStorage.setItem('navotia_cart', JSON.stringify({cart: []}))
    }

    return <CartContext.Provider value={{services, addToCart, removeFromCart, emptyCart, ttlCost, setCart}} >
        {children}
    </CartContext.Provider>
}