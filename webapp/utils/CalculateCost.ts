export const calculateCost = (
  transactionsPerMonth: number,
  humanMinutesPerTransaction: number,
  hoursPerRobot = 320
) => {
  const costPerRobot = 10000;
  const minutesPerMonth = transactionsPerMonth * humanMinutesPerTransaction;
  const hoursPerMonth = minutesPerMonth / 60;
  const hoursPerYear = hoursPerMonth * 12;
  const robotsPerYear = hoursPerYear / (hoursPerRobot * 12);
  return robotsPerYear * costPerRobot;
};
