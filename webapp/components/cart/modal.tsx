import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Button,
    Flex,
    Text, useToast, Heading, Divider
} from "@chakra-ui/react";
import React, { useContext } from 'react'

// COMPONENT
import CartItem from "./Item";
import { getUser } from "services/getUser";

// CONTEXT
import { CartContext } from "context/cart";
import { useRouter } from "next/router";

const CartModal: React.FC<{isOpen: boolean, onClose: VoidFunction, openCheckout: VoidFunction, setOrdersType: (ordersType: string) => void}> = ({isOpen, onClose, openCheckout, setOrdersType}) => {

    const {services: cart, ttlCost} = useContext(CartContext);
    const toast = useToast();
    const router = useRouter();

    const handleCheckout = (ordersType: string) => {
        const user = getUser();
        if(!user.userExist){
            toast({
                status: 'warning',
                title: 'Please login/create new account to checkout',
                isClosable: true
            });

            router.push('/auth')
            return;
        }
        setOrdersType(ordersType)
        openCheckout();
    }

    const renderCartItems = () => {
        const [marketPlaceOrders, productsAndServices] = cart.reduce((prev, curr) => {
            curr.isDirectOrder ? prev[1].push(curr) : prev[0].push(curr)
            return prev;
        }, [[], []])
        return <>
            <Text fontSize={22}>Marketplace</Text>
            {
                marketPlaceOrders.map((prod: any, index: number) => <CartItem
                    key={index} {...prod} />)
            }
            <Text float={'left'}>Total Cost: {`$${ttlCost.marketplace}`}</Text>
            <Flex alignItems={'flex-start'}>
                <Button colorScheme={'brand'} onClick={() => handleCheckout('marketplace')} disabled={marketPlaceOrders.length <= 0}>Checkout</Button>
            </Flex>
            <Divider />
            <Text fontSize={22} as={'h6'}>Products and Services</Text>
            {
                productsAndServices.map((prod: any, index: number) => <CartItem
                    key={index} {...prod} />)
            }
            <Text float={'left'}>Total Cost: {`$${ttlCost.productsAndServices}`}</Text>
            <Flex alignItems={'flex-start'}>
                <Button colorScheme={'brand'} onClick={() => handleCheckout('productsAndServices')} disabled={productsAndServices.length <= 0}>Checkout</Button>
            </Flex>
        </>
    }

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} size={'lg'}>
        <ModalOverlay />
        <ModalContent 
            bg={'black'} color={'white'}
        >
          <ModalHeader>Cart</ModalHeader>
          <ModalCloseButton />
          <ModalBody >
            <Flex flexDir={'column'} gap={'10px'}>
                {
                   cart.length > 0 
                    ? renderCartItems()
                    : <Text textAlign={'center'}>Cart is empty!</Text>
                }
            </Flex>
          </ModalBody>
                 
          <ModalFooter justifyContent={'space-between'}>
           <Flex alignItems={'flex-end'}>
               <Button variant="ghost"  mr={3} onClick={onClose}>
                 Close
               </Button>
           </Flex>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default CartModal;
