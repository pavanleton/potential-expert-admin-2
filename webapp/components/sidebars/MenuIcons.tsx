import {
  Box,
  Flex,
  Image,
  Text,
  IconButton,
  useDisclosure,
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { BiCircle } from "react-icons/bi";
import { useRouter } from "next/router";
import {HiOutlineChevronDoubleLeft} from 'react-icons/hi'

// SERVICES
import { getTypesBySuper } from "services/api/store";

const NavItems: React.FC<{ icon: string, name: string, id: number, closeSideBar: VoidFunction, isMenuVisible: boolean, toggleMainMenu: any }> = ({icon, name, id, closeSideBar, isMenuVisible, toggleMainMenu }) => {

  const router = useRouter();

  const handleRedirect = () => {
    router.query.catId = `${id}`;
    router.query.category = `${id}`;
    router.pathname = '/services'
    router.replace(router)
    closeSideBar()
  }

  const openSidebarWithDelay = () => {
    let timeout: any;

    const cb = () => {
      toggleMainMenu(true);
    };

    clearTimeout(timeout);

    timeout = setTimeout(cb, 500);
  };
  
  return (
    <>
      <Flex
        color={"white"}
        alignItems={"center"}
        gap={"2rem"}
        cursor={"pointer"}
        w={'100%'}
        py={".8rem"}
        px={"1rem"}
        bg={"transparent"}
        onClick={handleRedirect}
        borderRadius={'8px'}
        _hover={{
          bg: '#2d2d2d',  
          color: "#00e074",
          "& > div > img": {
            filter:
              "invert(29%) sepia(100%) saturate(380%) hue-rotate(84deg) brightness(89%) contrast(93%)",
          },
        }}
        onMouseOver={openSidebarWithDelay}
      >
        <Box w={"30px"} h={"20px"} >
          <Image
            src={
                icon
            }
            w={"100%"}
            h={"100%"}
          />
        </Box>
       { isMenuVisible && <Text noOfLines={2}>{name}</Text>}
      </Flex>
    </>
  );
};

const MenuIcons: React.FC<{ list: any[], isMenuVisible: boolean, toggleMainMenu: any }> = ({ list, isMenuVisible, toggleMainMenu }) => {
  const [menu, setMenu] = useState([]);

  const router = useRouter();

  const productSuperType = router.query?.superType || 1;

  const {
    isOpen: isSidebarOpen,
    onClose: closeSideBar,
    onOpen: openSideBar,
  } = useDisclosure();

  useEffect(() => {
    getTypesBySuper(productSuperType as string | number).then((res) => {
      setMenu(res?.data?.data?.attributes?.types?.data || []);
    });
  }, []);


  return (
    <>
      <Flex 
        flexDir={'column'}
        gap={".4rem"}          
        pos={'relative'} 
        h={'100vh'}
        w={'100%'}
        px={'.5rem'}
      >
        <Flex
        flexDir={"column"}
        gap={".4rem"}          
        w={'100%'}
       >
              {menu.map((elem: any, index: number) => (
                <NavItems 
                    key={index} 
                    icon={elem?.attributes?.svgIcon?.data?.attributes?.url || ''} 
                    name={elem?.attributes?.name || ''}
                    id={elem.id}
                    closeSideBar={closeSideBar}
                    isMenuVisible={isMenuVisible}
                    toggleMainMenu={toggleMainMenu}
                />
              ))}
      </Flex>
      </Flex>

     
    </>
  );
};

export default MenuIcons;
