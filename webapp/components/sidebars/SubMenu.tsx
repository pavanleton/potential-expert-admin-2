import {
  Box,
  Flex,
  Image,
  Text,
  IconButton,
  useDisclosure,
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
} from "@chakra-ui/react";
import { BiCircle } from "react-icons/bi";
import { FiMenu } from "react-icons/fi";
import { useRouter } from "next/router";
import { useContext } from "react";

// COMPONENTS
import MenuIcons from "./MenuIcons";

// CONTEXT
import { BreadcrumbsContext } from "context/breadCrumbs";

const NavItems: React.FC<{ navs: any[] }> = ({ navs }) => {
  const router = useRouter();

  const { addLevel } = useContext(BreadcrumbsContext);

  const fetchProducts = (id: string) => {
    addLevel('pdid', parseInt(`${id}`))
    router.query.pdId = `${id}`;
    router.pathname = "/services";
    router.replace(router);
  };

  return (
    <>
      <Flex flexDirection={"column"} width={'100%'}>
        {navs.map((category: any, index: number) => (
          <Flex
            key={index}
            bg={ `${router.query.pdId}` === `${category.id}` ? "#2D2D2D" : '#121212'}
            px={"1rem"}
            py={"10px"}
            borderRadius={"10px"}
            alignItems={"center"}
            gridGap={"2rem"}
            fontSize={"md"}
            cursor={"pointer"}
            width={'100%'}
            _hover={{
              transition: "all .4s linear",
              bg: `${router.query.pdId}` === `${category.id}` ? "#121212" : '#2D2D2D',
            }}
            onClick={() => fetchProducts(category.id)}
          >
            <Text>{category?.attributes?.name || ""}</Text>
          </Flex>
        ))}
      </Flex>
    </>
  );
};

const SubMenuSideBarSKU: React.FC<{ list: any[] }> = ({ list }) => {
  const {
    isOpen: isSidebarOpen,
    onClose: closeSideBar,
    onOpen: openSideBar,
  } = useDisclosure();

  const router = useRouter();

  return (
    <>
      <Drawer isOpen={isSidebarOpen} placement="left" onClose={closeSideBar}>
        <DrawerOverlay />
        <DrawerContent bg={"#121212"}>
          <DrawerCloseButton />
          <DrawerHeader>
            <Image
              src={"https://i.ibb.co/zX62Lff/NOVATIO-LOGO.png"}
              alt={"Novatio logo"}
            />
          </DrawerHeader>

          <DrawerBody>
            <NavItems navs={list} />
          </DrawerBody>
        </DrawerContent>
      </Drawer>

      <Flex
        d={{ base: "none", lg: "flex" }}
        bg={"#121212"}
        // p={"1rem"}
        h={"100vh"}
        flexDirection={"column"}
        w={'100%'}
      >
        <Flex px={"5px"} gridGap={"10px"} width={'100%'}>
          <Flex
            flexDir={"column"}
            alignItems={"center"}
            gap={".4rem"}
            h={'100vh'}       
          >
            
          </Flex>
          <Flex pt={"4rem"} px={"10px"} 
            width={'100%'} 
           >
            <NavItems navs={list || []} />
          </Flex>
        </Flex>
      </Flex>

      <Flex
        d={{ base: "flex", lg: "none" }}
        as={"aside"}
        bg={"#121212"}
        p={"2rem"}
        flexDirection={"column"}
        gridGap={"2rem"}
      >
        <Flex
          w={{ base: "150px", md: "200px" }}
          pl={{ base: "none", lg: "1rem" }}
          justifyContent={{ base: "space-between", lg: "flex-start" }}
        >
          <IconButton
            icon={<FiMenu />}
            aria-label={"Sidebar"}
            variant={"dark"}
            onClick={openSideBar}
            d={{ base: "block", lg: "none" }}
          />
          <Image
            src={"https://i.ibb.co/zX62Lff/NOVATIO-LOGO.png"}
            alt={"Novatio logo"}
          />
        </Flex>
      </Flex>
    </>
  );
};

export default SubMenuSideBarSKU;
