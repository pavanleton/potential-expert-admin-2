import {
  Box,
  Flex,
  Image,
  Text,
  IconButton,
  useDisclosure,
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
} from "@chakra-ui/react";
import { BiCircle } from "react-icons/bi";
import { FiMenu } from "react-icons/fi";
import { useRouter } from "next/router";
import { useContext } from "react";

// COMPONENTS
import MenuIcons from "./MenuIcons";

// CONTEXT
import { BreadcrumbsContext } from "context/breadCrumbs";

const NavItems: React.FC<{ navs: any[] }> = ({ navs }) => {
  const router = useRouter();

  const { addLevel } = useContext(BreadcrumbsContext);

  const fetchProducts = (id: string) => {
    addLevel('pdid', parseInt(`${id}`))
    router.query.category = id as string;
    router.pathname = "/servicesSKU";
    router.push(router);
  };

  return (
    <>
      <Flex flexDirection={"column"}>
        {navs.map((category: any, index: number) => (
          <Flex
            key={index}
            bg={"#121212"}
            px={"1rem"}
            py={"10px"}
            borderRadius={"10px"}
            alignItems={"center"}
            gridGap={"2rem"}
            fontSize={"md"}
            cursor={"pointer"}
            _hover={{
              transition: "all .4s linear",
              bg: "#2D2D2D",
            }}
            onClick={() => fetchProducts(category.attributes.id)}
          >
            <Text>{category?.attributes?.name || ""}</Text>
          </Flex>
        ))}
      </Flex>
    </>
  );
};

const SubMenuSideBarSKU: React.FC<{ list: any[] }> = ({ list }) => {
  const {
    isOpen: isSidebarOpen,
    onClose: closeSideBar,
    onOpen: openSideBar,
  } = useDisclosure();

  const router = useRouter();

  return (
    <>
      <Drawer isOpen={isSidebarOpen} placement="left" onClose={closeSideBar}>
        <DrawerOverlay />
        <DrawerContent bg={"#121212"}>
          <DrawerCloseButton />
          <DrawerHeader>
            <Image
              src={"https://i.ibb.co/zX62Lff/NOVATIO-LOGO.png"}
              alt={"Novatio logo"}
            />
          </DrawerHeader>

          <DrawerBody>
            <NavItems navs={list} />
          </DrawerBody>
        </DrawerContent>
      </Drawer>

      <Flex
        d={{ base: "none", lg: "flex" }}
        bg={"#121212"}
        p={"1rem"}
        h={"100vh"}
        flexDirection={"column"}
      >
        <Flex px={"5px"} gridGap={"10px"} >
          <Flex
            w={"50px"}
            flexDir={"column"}
            alignItems={"center"}
            gap={".4rem"}

            h={'100vh'}
            
          >
            <Flex w={"50px"} h={"50px"} mx={"1rem"}>
              <Image
                src={"https://i.ibb.co/JRv6Vrc/Novatio-Icon.png"}
                alt={"Novatio logo"}
                w={"100%"}
                h={"100%"}
                cursor={"pointer"}
                onClick={() => router.push("/home")}
              />
            </Flex>
            <Flex flexDir={"column"} alignItems={"center"} gap={".4rem"}>
              <MenuIcons list={list} />
            </Flex>
          </Flex>
          <Flex pt={"4rem"} px={"10px"} 
            borderLeft={'1px solid #000'}>
            <NavItems navs={list || []} />
          </Flex>
        </Flex>
      </Flex>

      <Flex
        d={{ base: "flex", lg: "none" }}
        as={"aside"}
        bg={"#121212"}
        p={"2rem"}
        flexDirection={"column"}
        gridGap={"2rem"}
      >
        <Flex
          w={{ base: "150px", md: "200px" }}
          pl={{ base: "none", lg: "1rem" }}
          justifyContent={{ base: "space-between", lg: "flex-start" }}
        >
          <IconButton
            icon={<FiMenu />}
            aria-label={"Sidebar"}
            variant={"dark"}
            onClick={openSideBar}
            d={{ base: "block", lg: "none" }}
          />
          <Image
            src={"https://i.ibb.co/zX62Lff/NOVATIO-LOGO.png"}
            alt={"Novatio logo"}
          />
        </Flex>
      </Flex>
    </>
  );
};

export default SubMenuSideBarSKU;
