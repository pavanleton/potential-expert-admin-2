import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  FormControl,
  FormLabel,
  Grid,
  useToast, Input
} from "@chakra-ui/react";
import { useRouter } from "next/router";

// SERVICES
import { createOrder } from "services/api/checkout";
import { getUser } from "services/getUser";

// CONTEXT
import { CartContext } from "context/cart";
import {useContext, useEffect, useState} from "react";

const Checkout: React.FC<{ isOpen: boolean; onClose: VoidFunction; ordersType: 'marketplace' | 'productsAndServices' }> = ({
  isOpen,
  onClose,
    ordersType
}) => {

  const [shipping, setShipping] = useState({
    fname:'',
    lname: '',
    address: '',
    address2: '',
    city: '',
    state: '',
    country: '',
    zip: '',
  })

  const toast = useToast()
  const router = useRouter();
  const {ttlCost, services: cart, emptyCart} = useContext(CartContext)

  const handlechange = (event: any) => {
    return setShipping({...shipping, [event.target.id]: event.target.value})
  }
useEffect(() => {
  console.log({ cart })
}, [])
  const processCheckout = async() => {
    try{

        const user = getUser();
        if(!user.userExist){
          toast({
            status: 'warning',
            title: 'Please login/create new account to checkout',
            isClosable: true
          })

          router.push('/auth')
          return;
        }
        const directOrderSKUs = cart.filter(ord => ord.isDirectOrder).map(ord => ord.sku)
        const {data} = await createOrder(shipping, cart.map((prod) => ({
          id: prod.id,
          name: prod.name,
          sku: prod.sku,
          transactionsPerMonth: prod.transactionsPerMonth,
          humanMinutesPerTransaction: prod.humanMinutesPerTransaction,
        })), directOrderSKUs)
        setShipping({
          fname:'',
          lname: '',
          address: '',
          address2: '',
          city: '',
          state: '',
          country: '',
          zip: '',
        })
        toast({
          status: 'success',
          title: 'Order Placed!',
          isClosable: true
        })

        window.location.assign(data.data.sessionUrl);
        let ordersInStore: any = localStorage.getItem('navotia_orders');
        const userId = getUser().user.id

      const itemIds = cart.map((prod) => prod.id);
      const purchasedItems = localStorage.getItem('novatio_purchased_items');
        if (purchasedItems) {
          localStorage.setItem('novatio_purchased_items', JSON.stringify([
            // @ts-ignore
            ...purchasedItems,
              ...itemIds
          ]));
        } else
          localStorage.setItem('novatio_purchased_items', JSON.stringify(itemIds))
        if(!ordersInStore){
          localStorage.setItem('navotia_orders', JSON.stringify([{
            user: userId,
            orders: [data]
          }]))

          onClose()
          emptyCart()

          return 
        }
        ordersInStore = JSON.parse(ordersInStore);
        ordersInStore = ordersInStore.map((order: any) => {
          if(order.user === userId){
            order.orders = [...order.orders, data]
            return order
          }
          return order
        })

        localStorage.setItem('navotia_orders', JSON.stringify(ordersInStore))
        
        onClose()
        emptyCart()
        return
    }catch(error: any){
      console.log(error);
    }
  }


  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} size={'5xl'}>
        {

        }
        <ModalOverlay />
        <ModalContent bg={'black'} color={'white'}>
          <ModalHeader>Checkout</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl mb={'1rem'}>
              <FormLabel htmlFor="contact-infp" fontWeight={600}>Contact Information</FormLabel>
              <Input variant={'solid'} bg={'#3A3A3A'} id="contact-infp" type="tel" placeholder="Email or mobile phone number" />
            </FormControl>

            <FormControl>
              <FormLabel htmlFor="contact-infp" fontWeight={600}>Shipping Address</FormLabel>
              <Grid gridTemplateColumns={{base: '1fr', lg: 'repeat(3, 1fr)'}} gridGap={'1rem'}>
                <Input variant={'solid'} bg={'#3A3A3A'} id="fname" type="text" placeholder="First name" gridColumn={'span 2'} value={shipping.fname} onChange={handlechange} />
                <Input variant={'solid'} bg={'#3A3A3A'} id="lname" type="text" placeholder="Last name" value={shipping.lname}  onChange={handlechange}/>
                <Input variant={'solid'} bg={'#3A3A3A'} id="address" type="text" placeholder="Address" gridColumn={'span 3'}  value={shipping.address} onChange={handlechange}/>
                <Input variant={'solid'} bg={'#3A3A3A'} id="address2" type="text" placeholder="Apparment, Suite, etc" gridColumn={'span 3'}  value={shipping.address2} onChange={handlechange}/>
                <Input variant={'solid'} bg={'#3A3A3A'} id="city" type="text" placeholder="City" gridColumn={'span 3'}  value={shipping.city} onChange={handlechange}/>
                <Input variant={'solid'} bg={'#3A3A3A'} id="country" type="text" placeholder="Counry/Region" gridColumn={'span 1'}  value={shipping.country} onChange={handlechange}/>
                <Input variant={'solid'} bg={'#3A3A3A'} id="state" type="text" placeholder="State"  gridColumn={'span 1'}  value={shipping.state} onChange={handlechange}/>
                <Input variant={'solid'} bg={'#3A3A3A'} id="zip" type="number" placeholder="Zip Code"  gridColumn={'span 1'}  value={shipping.zip} onChange={handlechange}/>
              </Grid>
            </FormControl>
          </ModalBody>

          <ModalFooter justifyContent={'flex-start'}>
            <Button colorScheme={"brand"} mr={3} onClick={processCheckout}>
            Continue to shipping
            </Button>
            <Button variant={"ghost"} colorScheme={"brand"} onClick={onClose}>Return to cart</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default Checkout