import { Flex, Box, Text, Image } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React from 'react'

const Cards: React.FC<{image: string, name: string, id: number, handleClick: (id: number) => void}> = ({image, name, id, handleClick}) => {
    
    return <>
        <Flex  
            h={{base: '400px', md: '500px', lg: '500px'}} 
            pos={'relative'}
            w={'100%'} 
            cursor={'pointer'}
            borderRadius={'8px'}
            onClick={() => handleClick(id)}
        >
            <Box 
                pos={'absolute'} 
                background={'linear-gradient(180deg, #121212 8.65%, rgba(18, 18, 18, 0) 48.23%);'} 
                w={'100%'} 
                h={'100%'}  
                inset={0}
                zIndex={10}
                transform={'rotate(-180deg)'}
                borderRadius={'8px'}
                _hover={{
                    transition: 'all .4s linear',
                    background: 'rgba(18, 18, 18, 0.31)',
                }}
            />  

            <Box w={'100%'} h={'100%'} inset={0}>
                <Image 
                    src={image} 
                    w={'100%'} h={'100%'} 
                    objectFit={'cover'} borderRadius={'8px'}

                />
            </Box>  


            <Text pos={'absolute'} textAlign={'center'} bottom={'20px'} w={'100%'} fontSize={'lg'} fontWeight={500} zIndex={20}>{name}</Text>

        </Flex>
    </>
}

export default Cards;