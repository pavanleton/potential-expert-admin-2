import { Flex, Input } from "@chakra-ui/react";
import { BiSearch } from 'react-icons/bi';
import React from "react";

const SearchBar: React.FC = () => {
    return <>
        <Flex bg={'#121212'} px={'2rem'} py={'15px'} borderRadius={'10px'} gridGap={'10px'} alignItems={'center'}>
            <Input placeholder='Search Services' type={'search'} variant={'unstyled'} _placeholder={{color: "#777777"}} />
            <BiSearch />
        </Flex>
    </>;
}

export default SearchBar;