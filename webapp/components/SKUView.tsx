import React from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Flex,
  Text,
  Heading,
  Button,
} from "@chakra-ui/react";

const SKUView: React.FC<{
  isOpen: boolean;
  onClose: VoidFunction;
  data: any;
}> = ({ isOpen, onClose, data }) => {
	const { image, name, description, isInCart, id, sku, cost, unitOfMeasure } = data;

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} size={"5xl"}>
        <ModalOverlay backdropFilter='blur(10px) hue-rotate(90deg)' />
        <ModalContent bg={"#121212"} color={"white"} border={'1px solid #25A146'} >
          <ModalCloseButton />
          <ModalBody>
            <Flex justifyContent={"space-between"} padding={"2rem"}>
              <div
                style={{ width: "50%", height: "70vh", background: "#272727" }}
              ></div>
              <Flex
                width={"40%"}
                // padding={"4rem 0"}
								justifyContent={'center'}
                flexDirection={"column"}
                gap={"1rem"}
              >
                <Heading as="h1" size={"md"}>
                  {name}
                </Heading>
                <Text color={"#D2D2D2"}>
                  {description}
                </Text>
                <Text
                  border={"1px solid #232323"}
                  borderWidth={"1px 0 1px 0"}
                  padding={"0.5rem 0"}
                  fontSize={"1.2rem"}
                >
                  {id}
                </Text>
                <Text>
                  <Heading as="h2" size={"md"} color={'#25A146'} >
                    {cost}
                  </Heading>
                  {unitOfMeasure}
                </Text>
                <Flex justifyContent={"space-between"}>
                  <Button width={"45%"} bg={"black"} _hover={{bg: "#121212"}} >
                    View
                  </Button>
                  <Button width={"45%"} colorScheme={"brand"}>
                    Buy Now
                  </Button>
                </Flex>
              </Flex>
            </Flex>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default SKUView;
