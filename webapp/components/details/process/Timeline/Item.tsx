import {
  Box,
  Text,
  Flex,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  Input
} from "@chakra-ui/react";
import { useContext, useEffect, useState } from "react";

// CONTEXT
import { ProcessContext, ProcessType, SubProcessType } from "context/process";


const Item: React.FC<{title: string, isChecked?: boolean, id: number, description: string}> = ({title, children, isChecked, id, description}) => {
  
  const [value, setValue] = useState('');

  const {toggleSubProcess, updateSubProcessContent, setIsUnsaved} = useContext(ProcessContext)

  useEffect(() => {
    setValue(description);
  }, [description])

  useEffect(() => {
  
    const debounceUpdate = setTimeout(() => {
      updateSubProcessContent(id, value);
    }, 1000); 


    return () => {
      clearTimeout(debounceUpdate);
    };


  }, [value])
  
  const checkProcess = () => {
    toggleSubProcess(id)
  }

  const handleChange = (event: any) => {
    setValue(event.target.value)
    setIsUnsaved.on();
  }
  
  return (
    <>
      <Box
        padding={"10px 40px"}
        pos={"relative"}
        bg={"inherit"}
        w={"100%"}
        _after={{
          content: '""',
          pos: "absolute",
          w: "17px",
          h: "17px",
          left: 0,
          bg: isChecked ? "brand.600" : "#000",
          border: "2px solid",
          borderColor: "brand.600",
          top: "15px",
          borderRadius: "50%",
          zIndex: 1,
        }}
        onClick={checkProcess}
      >
        <Flex
          padding={"10px 15px"}
          bg={"#282828"}
          pos={"relative"}
          borderRadius={"6px"}
        >
          <Accordion defaultIndex={[0]} allowMultiple w={"100%"}>
            <AccordionItem w={"100%"} border={"none"}>
              <AccordionButton w={"100%"} 
              fontWeight={700}
              _focus={{
                  outline: 'none'
              }}
              _expanded={{
                  color: 'brand.600'  
              }}
              >
                {title}
              </AccordionButton>
              <AccordionPanel pb={4} itemScope>
                <Input fontSize={"sm"} fontWeight={400} variant={'unstyled'} w={'100%'} h={'100%'} value={value} onChange={handleChange} />
              </AccordionPanel>
            </AccordionItem>
          </Accordion>
        </Flex>
      </Box>
    </>
  );
};

export default Item;
