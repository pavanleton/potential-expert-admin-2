import { Flex, Text, Checkbox } from "@chakra-ui/react";
import React, { useContext } from "react";

// CONTEXT
import { ProcessContext } from "context/process";

const Item: React.FC<{
  title: string;
  id: number;
  isChecked?: boolean;
  isSelected?: boolean;
  subProcess: any[];
}> = ({ title, isChecked, isSelected, subProcess, id }) => {
  const { updateSelection, toggleProcess } = useContext(ProcessContext);

  const isCompleted = React.useMemo(
    () =>
      subProcess.length > 0
        ? subProcess.every((process: { isCompleted: boolean }) =>
            Boolean(process.isCompleted)
          )
        : false,
    [subProcess]
  );

  React.useEffect(() => {
    toggleProcess(isCompleted);
  }, [isCompleted]);

  const handleClick = () => updateSelection(id);

  return (
    <>
      <Flex
        border={"2px solid"}
        borderColor={isSelected ? "brand.600" : "#121212"}
        p={"1rem"}
        gridGap={"10px"}
        borderRadius={"10px"}
        w={"100%"}
        bg={"#121212"}
        id={`${id}`}
        onClick={handleClick}
        cursor={"pointer"}
      >
        <Checkbox
          colorScheme={"brand"}
          isChecked={isChecked || isCompleted}
          disabled
          borderColor={"brand.600"}
        />

        <Text
          color={"brand.600"}
          fontWeight={700}
          id={title}
          onClick={handleClick}
        >
          {title}
        </Text>
      </Flex>
    </>
  );
};

export default Item;
