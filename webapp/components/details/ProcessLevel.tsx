import { Flex, Grid, Box, Button } from "@chakra-ui/react";
import React, {useContext, useEffect} from "react";

// COMPONENTS
import ProcessItem from "./process/Item";
import TimeLine from "./process/Timeline";

// CONTEXT
import { ProcessContext, ProcessType, SubProcessType } from "context/process";

const ProcessLevel: React.FC = () => {

  const {selectedProcess, process, subProcess, updateSelection, saveSubProcess} = useContext(ProcessContext);

  useEffect(() => {
      // updateSelection(1)
    if(process.length)
      updateSelection(process[0]?.id)

  }, []);

  return (
    <>
      <Grid
        gridTemplateColumns={{ base: "1fr", md: "1fr 2fr" }}
        gridGap={"1rem"}
      >
        <Flex
          flexDirection={{ base: "row", md: "column" }}
          gridGap={"1rem"}
          overflowX={"scroll"}
          
          css={{
            '&::-webkit-scrollbar':{
              display: 'none'
            }
          }}
        >
            {
                process.map((pcs: ProcessType) => 
                    <ProcessItem
                          {...pcs}
                          key={pcs.id}
                          isChecked={pcs.isCompleted}
                          isSelected={selectedProcess === pcs.id}
                          subProcess={subProcess.filter((subpcs: SubProcessType) => subpcs.pid === pcs.id)}
                    />
                )
            }

        </Flex>
        <Box px={"1.5rem"} py={"2rem"} bg={"#121212"} borderRadius={"10px"}>
          <TimeLine />
          <Flex justify={'flex-end'} gap={'10px'}>
            <Button colorScheme={'dark'}>Help</Button>
            <Button colorScheme={'brand'} onClick={saveSubProcess}>Save</Button>
          </Flex>
        </Box>
      </Grid>
    </>
  );
};

export default ProcessLevel;
