import {Box, Button, Grid} from "@chakra-ui/react";
import dynamic from "next/dynamic";
import {useCallback, useContext, useEffect, useState} from "react";
import {useRouter} from "next/router";

// @ts-ignore
import md2json from "md-2-json";

// COMPONENTS
import TextCard from "./stats/TextCard";
import IconCard from "./stats/IconCard";
import Benefits from "./stats/Benefits";
import Summary from "./stats/Summary";

// UTILS
import {calculateCost as calculatePrice} from 'utils/CalculateCost'

// CONTEXT
import {CartContext} from "context/cart";

// ASSET
import Timeline from "asset/svg/TimeLine";

// SERVICEs
import {getProduct} from "services/api/store";

// CSR imports
const Chart = dynamic(() => import("./stats/Chart"), {
  ssr: false,
});

const ExecutiveLevel: React.FC = () => {
  const [product, setProduct] = useState({
    complexity: "Low",
    industryUse: "80%",
    averageHandlineTime: "2 Min",
    timeline: "4-5 Weeks",
    benefits: "",
    name: '',
    id: 0,
    summary:
      "The Bot will monitor all approved tickets for access to a tool. When approved the bot will enter the ERP/application and apply new roles.",
  });
  const [txnM, setTxnM] = useState(2500);
  const [manHandling, setManHandling] = useState(0);
  const [avgCost, setAvg] = useState(`0`);

  const router = useRouter();
  const productId = router.query.name;

  const {addToCart, removeFromCart, services: cart, setCart } = useContext(CartContext)

  const calculateCost = useCallback(calculatePrice, [manHandling, txnM]);

  useEffect(() => {
    getProduct(productId as string | number)
      .then((respp) => {
      setProduct({id: respp.data.data.id,...respp.data.data.attributes});
      setManHandling(respp.data.data.attributes.manualHandlingTime || 10);
      setAvg(`${respp.data.data.attributes.price}`);
    })
    .catch((err: any) => console.log(err))
  }, [productId]);

  useEffect(() => {
    const price = calculateCost(txnM, manHandling);
    // @ts-ignore
    setAvg(Math.floor(price));
    if (cart.length) {
      const item = cart.find(c => c.id === Number(productId));
      console.log({ cart, item })
      if (item) {
        item.transactionsPerMonth = txnM;
        item.humanMinutesPerTransaction = manHandling;
        item.price = price;

        const updatedCartState = [
          ...cart.filter(c => c.id !== Number(productId)),
          item
        ];
        setCart(updatedCartState);
      }
    }
  }, [calculateCost, txnM, manHandling]);

  const defaultBenefits = `unavailable`

  return (
    <>
      <Grid
        w={"100%"}
        h={"100%"}
        gridTemplateColumns={{
          base: "1fr",
          lg: "repeat(4, 1fr)",
        }}
        gridGap={"1rem"}
      >
        <Box gridColumn={{ lg: "span 4" }}>
          <Summary
            heading="Project Summary"
            value={
              product?.summary ||
              "The Bot will monitor all approved tickets for access to a tool. When approved the bot will enter the ERP/application and apply new roles."
            }
          />
        </Box>
        <TextCard heading={"Complexity"} value={product?.complexity || "Low"} />
        <TextCard
          heading="Industry Use"
          value={product?.industryUse || "80%"}
        />
        <Box gridColumn={{ lg: "span 2" }}>
          <TextCard
            heading="Avg Handling time by Bot"
            value={product?.averageHandlineTime || "2 Min"}
            subValue="( Based on our exp )"
          />
        </Box>
        <Box gridColumn={{ lg: "span 2" }}>
          <TextCard
            heading="Timeline EST"
            value={product?.timeline || "4-5 Weeks"}
          />
        </Box>
        <TextCard
          heading="Transactions /Month"
          value={`${txnM}`}
          editable
          handlechange={setTxnM}
        />
        <Box gridRow={{ lg: "span 2" }}>
          <IconCard
            heading="Manual Handling Time"
            value={`${manHandling}`}
            Icon={Timeline}
            editable
            handlechange={setManHandling}
          />
        </Box>
        <Box gridColumn={{ lg: "span 2" }} gridRow={{ lg: "span 5" }}>
          <Benefits
            details={Object.entries(md2json.parse(product?.benefits || defaultBenefits))}
          />
        </Box>
        <TextCard heading="Avg Manual Cost" value={`$${avgCost}`} />
        <Box gridColumn={{ lg: "span 2" }} gridRow={{ lg: "span 2" }}>
          <Chart />
        </Box>
        <Box gridColumn={{ lg: "span 4" }}>
        </Box>
        <Box gridColumn={{ lg: "span 2" }}>
          <Button
            colorScheme={"brand"}
            w={"100%"}
            onClick={ cart.some((p) => p.id === product.id) ? () => removeFromCart(`${product.id}`) :  () => addToCart({
              ...product,
              transactionsPerMonth: txnM,
              humanMinutesPerTransaction: manHandling,
              price: avgCost,
            })}
          >
            {cart.some((p) => p.id === product.id) ? 'Remove from cart' :"Add to cart"}
          </Button>
        </Box>
        <Box gridColumn={{ lg: "span 2" }}>
          <Button
            onClick={() => router.push("/auth")}
            bg={"#282828"}
            w={"100%"}
            color={"white"}
            _hover={{ bg: "#282828" }}
          >
            Calculate ROI
          </Button>
        </Box>
      </Grid>
    </>
  );
};

export default ExecutiveLevel;
