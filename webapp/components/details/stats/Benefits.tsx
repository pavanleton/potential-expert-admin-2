import { Flex, Heading, Box, Text } from "@chakra-ui/react";
import React from "react";
import { GoPrimitiveDot } from "react-icons/go";

const Benefits: React.FC<{ details: any[] }> = ({ details }) => {

  return (
    <>
      <Flex
        bg={"#121212"}
        p={"1.5rem"}
        flexDir={"column"}
        alignItems={"flex-start"}
        borderRadius={"10px"}
        gridGap={"12px"}
      >
        <Heading
          as={"h6"}
          size={"sm"}
          d={"flex"}
          color={"brand.600"}
          alignItems={"center"}
          gridGap={"5px"}
        >
          <GoPrimitiveDot /> Benefits/Drivers
        </Heading>

        <Flex flexDirection={"column"} gridGap={"12px"}>
          {details.map((benefit: any[], index: number) => {
            return (
              <Flex
                gridGap={"2px"}
                flexDir={"column"}
                p={"1rem"}
                bg={"#282828"}
                w={"100%"}
                borderRadius={"10px"}
                key={index}
              >
                <Heading
                  as={"h6"}
                  size={"sm"}
                  d={"flex"}
                  color={"white"}
                  alignItems={"center"}
                  gridGap={"5px"}
                >
                  <Text color={"brand.600"}>
                    {" "}
                    <GoPrimitiveDot />
                  </Text>{" "}
                  {benefit[0]}
                </Heading>
                <Text fontSize={"sm"}>{benefit[1].raw}</Text>
              </Flex>
            );
          })}
        </Flex>
      </Flex>
    </>
  );
};

export default Benefits;
