import { Flex, Heading, Text } from "@chakra-ui/react";
import React from "react";

const Summary: React.FC<{ heading: string; value: string, subValue?: string }> = ({
  heading,
  value,
  subValue
}) => {
  return (
    <>
      <Flex bg={"#121212"} p={"1.5rem"} flexDir={'column'} alignItems={'flex-start'} borderRadius={'10px'}>
        <Heading as={"h6"} size={"sm"} color={"brand.600"}>
          {heading}
        </Heading>
        <Text as={"h4"} size={"sm"}>
          {value}
        </Text>
      </Flex>
    </>
  );
};

export default Summary;
