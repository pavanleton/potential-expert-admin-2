import { Flex, Heading, Input, Text } from "@chakra-ui/react";
import React from "react";

const TextCard: React.FC<{ heading: string; value: string, subValue?: string, editable?: boolean, handlechange?: any }> = ({
  heading,
  value,
  subValue,
  editable,
  handlechange
}) => {
  return (
    <>
      <Flex bg={"#121212"} p={"1.5rem"} flexDir={'column'} alignItems={'flex-start'} borderRadius={'10px'}>
        <Heading as={"h6"} size={"sm"} color={"brand.600"}>
          {heading}
        </Heading>
        <Flex>
          <Input value={value} readOnly={!editable} contentEditable={editable} variant={'unstyled'} fontSize={'3xl'} fontWeight={'bold'} color={'white'} onChange={(e: any) => handlechange(e.target.value)} />
          <Text contentEditable={false} fontSize={'xs'}>{subValue}</Text>
        </Flex>
      </Flex>
    </>
  );
};

export default TextCard;
