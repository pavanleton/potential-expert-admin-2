import {Box, Image, Text, Button, Flex, Heading, Divider } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useContext, useState } from "react";


// CONTEXT
import { CartContext } from "context/cart";
import { BreadcrumbsContext } from "context/breadCrumbs";
import ViewModal from 'components/SKUView';



const ServicesCard: React.FC<any> = (props) => {
  const { image, name, description, isInCart, id, sku, cost, unitOfMeasure } = props;

  const router = useRouter();
  const { addToCart, removeFromCart } = useContext(CartContext);
  const { addLevel } = useContext(BreadcrumbsContext);
  const [displayModal, setDisplayModal] = useState(false);

  const redirectToDetails = () => {
    addLevel('prod', parseInt(`${id}`));
    router.pathname = `/details/${id}`;
    router.replace(router);
  };

  return (
    <>
      <Box position={"relative"} bg={"#272727"} rounded={"lg"} border={'1px solid rgba(255, 255, 255, 0.1)'}>
        <Box h={"200px"} w={"100%"}>
          <Image
            objectFit={"cover"}
            h={"100%"}
            w={"100%"}
            src={image || 'https://i.ibb.co/qxPdhwz/Group-1.png'}
            alt={"Cover image"}
            rounded={"lg"}
            boxShadow={"lg"}
          />
        </Box>

        <Flex
          w={"100%"}
          gridGap={"10px"}
          flexDirection={"column"}
          bottom={"10px"}
          zIndex={20}
          bg={"#121212"}
          p={"1rem"}
        >
          <Heading size={"md"} as={"h5"} noOfLines={2}>
            {name}
          </Heading>
          <Text noOfLines={3}>{description}</Text>
          { props.sku && <>
            <Divider style={{}}
                    color={'black'}/>
            <Flex
            w={"100%"}
            // gridGap={"10px"}
            flexDirection={"column"}
            bottom={"10px"}
            zIndex={20}
            bg={"#121212"}
            p={"1rem"}
            >
            <Heading size={"md"} as={"h5"}>
            SKU: {sku}
            </Heading>

            </Flex>
            <Divider colorScheme={'brand'} />
            <Flex
            w={"100%"}
            // gridGap={"10px"}
            flexDirection={"column"}
            bottom={"10px"}
            zIndex={20}
            bg={"#121212"}
            p={"1rem"}
            >
            <Heading color={'#FF3131'} size={"md"} as={"h5"}>
          {cost}
            </Heading>
            <Text color={'cadetblue'}>
          {unitOfMeasure}
            </Text>
            </Flex>
          </>
          }
          <Flex  marginTop={'15px'} gap={"5px"}>

            {  <Button
                w={props.sku ? "50%" : '100%'}
                bg={"#000"}
                _hover={{bg: "#121212"}}
                onClick={() => sku ? setDisplayModal(true) : redirectToDetails()}
            >
              View
            </Button>}
            {props.sku && <Button
                w={props.sku ? "50%" : '100%'}
                colorScheme={"brand"}
                onClick={
                  isInCart
                      ? () => removeFromCart(`${id}`)
                      : () => addToCart({
                        ...props,
                        price: parseInt(cost.replace( /^\D+/g, '').replace(',','')),
                        isDirectOrder: Boolean(props.sku),
                      })
                }
            >
              {isInCart ? "Remove from cart" : "Add to cart"}
            </Button>}
          </Flex>
        </Flex>
      </Box>
      <ViewModal isOpen={displayModal} onClose={() => setDisplayModal(false)} data={props} />
    </>
  );
};

export default ServicesCard;
