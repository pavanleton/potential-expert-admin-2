import { Flex, Heading, Text, IconButton } from "@chakra-ui/react";
import {FiTrash2} from 'react-icons/fi'
import { useContext } from "react";
import dayjs from "dayjs";

// CONTEXT
import { CartContext } from "context/cart";
import {useRouter} from "next/router";

const OrderItem: React.FC<any> = (props) => {
  const router = useRouter();

  return (
    <>
      <Flex onClick={() => router.push(`/details/${props?.attributes?.productId}`)} cursor={'pointer'} gap={'1rem'} border={'1px solid white'} w={'100%'} justify={'space-between'} alignItems={'center'} p={'10px'} borderRadius={'8px'}>
        <Flex flexDir={'column'} gap={'10px'} w={'85%'} >
          <Heading size={'sm'} as={'h5'} noOfLines={1}>{props?.attributes?.productName|| ''}</Heading>

          <Text noOfLines={2} isTruncated fontSize={'xs'} w={'100%'} fontWeight={500}>
            {dayjs(props?.attributes?.createdAt).format('DD MMM YY hh:mm') || ''}
          </Text>
          
          <Text>
            {`$${props?.attributes?.price || 0}`}
          </Text>
        </Flex>

      </Flex>
    </>
  );
};

export default OrderItem;
