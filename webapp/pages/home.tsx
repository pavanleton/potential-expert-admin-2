import { Container, Grid, toVarReference, useToast } from "@chakra-ui/react";
import { NextPage } from "next";
import {useContext, useEffect, useState} from "react";
import { useRouter } from "next/router";

// COMPONENTS
import SearchBar from "components/home/SearchBar";
import Filter from "components/home/Filter";
import Cards from "components/home/Cards";
import Navbar from "components/Navbar";

// CONTEXT
import { BreadcrumbsContext } from "context/breadCrumbs";

// SERVICES
import {getSuperTypes} from "services/api/store";

const Home: NextPage = () => {
  const [serviceTypes, setServiceTypes] = useState([])

  const toast = useToast()
  const router = useRouter();

  const {addLevel, clearAllLevels} = useContext(BreadcrumbsContext)

  const redirectToCategories = (id: number) => {
    clearAllLevels();
    addLevel('st', id);
    router.push(`/product-types?superType=${id}`)
  }

  useEffect(() => {
    getSuperTypes()
    .then(r => {
      console.log(r);
      
      setServiceTypes(r?.data?.data || [])})
    .catch(err => toast({
      status: 'error',
      title: 'Something went wrong',
      description: err.message,
      isClosable: true
    }))
  }, []);

  return (
    <>
      <Container maxW={"container.xl"} my={"2rem"}>
        <Navbar />

        <Grid my={'2rem'} gridTemplateColumns={{base: '1fr', md: '2fr 1fr', lg: '3fr 1fr'}} gridGap={'1rem'}>
            <SearchBar/>
            <Filter />
        </Grid>

        <Grid my={'2rem'} justifyContent={'center'} gridTemplateColumns={{base: '1fr', md: 'repeat(2, 1fr)', lg: 'repeat(4, 1fr)'}} gridGap={'1rem'}>
            {
              serviceTypes.map((services: any) =>            
                <Cards 
                  key={services.id}
                  image={services?.attributes?.coverImage?.data?.attributes?.url || ''} 
                  name={services?.attributes?.name || ''} 
                  id={services.id}
                  handleClick={redirectToCategories}
               />
            )
            }
            
        </Grid>
      </Container>
    </>
  );
};

export default Home;
