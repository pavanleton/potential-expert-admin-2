import {
    Flex,
    Box,
    FormControl,
    FormLabel,
    Input,
    Checkbox,
    Stack,
    Link,
    Button,
    Heading,
    Text,
    useColorModeValue, Image, AlertIcon, Alert, Spinner, useToast
} from '@chakra-ui/react';
import {useEffect, useState} from "react";
import {login, register} from "services/api/config";
import {useRouter} from "next/router";
import {verifyPayment} from "services/api/checkout";

export default function VerifyPayment() {

    const router = useRouter();
    const toast = useToast();
    const [isLoading, setLoading] = useState(true);
    const [paymentStatus, setPaymentStatus] = useState('');
    const verify = async () => {
        // @ts-ignore
        const orders = router.query.orders.split(',').map(i => Number(i));
        try {
            console.log({ orders })
            const { data } = await verifyPayment(orders);
            if (data === 'payment_complete') {
                setLoading(false);
                setPaymentStatus('success')
                toast({
                    status: 'success',
                    title: 'Payment Verified!',
                    isClosable: false,
                    position: 'top',
                    duration: 3000,
                    onCloseComplete: () => router.push('/home'),

                })
            } else throw '';
        } catch (e) {
            setLoading(false);
            setPaymentStatus('failed')

        }
    }
    useEffect(() => {

        setTimeout(() => {
            if(router.query?.orders)
            verify();
        }, 1000);

    }, [router.query])

    return (
        <Flex
            minH={'100vh'}
            justify={'center'}
            bg={useColorModeValue('black.50', 'white.800')}>

            <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={12}>
                {/* eslint-disable-next-line @next/next/no-img-element */}
                <img
                    width={'100%'}
                    src={"https://i.ibb.co/DfkckKP/NOVATIO.png"}
                    alt={"Novatio logo"}
                />
                <Stack align={'center'}>
                    { isLoading &&
                        <> <Text fontSize={25}>
                        Verifying your payment
                    </Text>

                    {/*// @ts-ignore*/}
                        <Spinner
                        style={{
                        marginTop: '45px'
                    }}

                        thickness='2px'
                        speed='0.65s'
                        color='green.500'
                        size='xl'
                        />
                        </>
                    }
                    {
                        paymentStatus && <Text fontSize={25}>
                            {paymentStatus === 'success' ? 'You are being redirected': 'You\'ve cancelled the order. \n If you think this is an error, please contact our support.'}
                        </Text>
                    }
                </Stack>
            </Stack>
        </Flex>
    );
}