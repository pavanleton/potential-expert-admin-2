import { NextPage } from "next";
import {
  Tabs,
  TabList,
  TabPanels,
  Tab,
  TabPanel,
  Box,
  Flex,
  Button,
    Heading,
    Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useEffect, useState, useContext } from "react";

// COMPONENTS
import ExecutiveLevel from "components/details/ExecutiveLevel";
import ProcessLevel from "components/details/ProcessLevel";

// CONTEXT
import { ProcessContext } from "context/process";

// LAYOUT
import ServicesLayout from "layout/services";

// TYPES
import PageWithLayoutType from "types/pageWithLayout";
import { getUser } from "services/getUser";
import {getUserOrders} from "services/api/checkout";
import {log} from "util";

const Details: NextPage = () => {
  const [tabIndex, setTabIndex] = useState<0 | 1>(0);
  const [hasBoughtTheProduct, setHasBoughtTheProduct] = useState(false);

  const router = useRouter();
  const productId = router.query.name;

  const {saveSubProcess, triggerRefetch} = useContext(ProcessContext)
  const [isAdminView, setIsAdminView] = useState(false)


  useEffect(() => {
    const isAdmin = router.query.adminView === 'true';
    if (isAdmin && router.query.userId)
      triggerRefetch()
    setIsAdminView(isAdmin);
  }, [router.query])

  const handleTabsChange = (index: number) => {
    if (index === 1)
      getUserOrders().then(res => {
        setHasBoughtTheProduct(res.data.data.some((ord: { id: number; }) => ord.attributes.productId == productId))
      })
    setTabIndex(index as 0 | 1);
  };

  return (
    <>
      <Box mt={"2rem"} mx={"3rem"}>
        <Tabs variant="unstyled" index={tabIndex} onChange={handleTabsChange}>
          <TabList gridGap={"1rem"} justifyContent={"space-between"}>
            <Flex gap={"1rem"}>
              {
                !isAdminView && <Tab
                  border={"2px solid #000"}
                  bg={"#282828"}
                  color={"white"}
                  borderRadius={"8px"}
                  _selected={{borderColor: "#25A146"}}
              >
                Executive Level
              </Tab> }
              <Tab
                disabled={true}
                border={"2px solid #000"}
                bg={"#282828"}
                color={"white"}
                borderRadius={"8px"}
                _selected={{ borderColor: "#25A146" }}
              >
                Process Level
              </Tab>
            </Flex>
            {true && tabIndex === 1 && (
              <Flex gap={"10px"}>
                <Button colorScheme={"dark"}>Help</Button>
                <Button colorScheme={"brand"} onClick={saveSubProcess}>Save</Button>
              </Flex>
            )}
          </TabList>
          <TabPanels>{
            !isAdminView &&
            <TabPanel>
              <ExecutiveLevel/>
            </TabPanel>}
            <TabPanel>
              {
                 true ?
                    <ProcessLevel/>
                    :
                    <Box >                     
                      <Alert
                        status='error'
                        variant='subtle'
                        flexDirection='column'
                        alignItems='center'
                        justifyContent='center'
                        textAlign='center'
                        height='200px'
                        bg={'none'}

                      >
                        <AlertIcon boxSize='40px' mr={0} />
                        <AlertTitle mt={4} mb={1} fontSize='xl'>
                          Purchase required!
                        </AlertTitle>
                        <AlertDescription maxWidth='sm' fontSize='lg'>
                          Please purchase this product to see the Process Level
                        </AlertDescription>
                      </Alert>
                    </Box>
              }
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </>
  );
};

(Details as PageWithLayoutType).layout = ServicesLayout;

export default Details;
