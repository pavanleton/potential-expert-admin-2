import react, {useEffect, useState} from 'react';
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    Stack,
    Heading,
    Container
} from '@chakra-ui/react'
import Navbar from '../../components/Navbar';
import {getOrders} from "services/api/admin";
import {useRouter} from "next/router";


const Orders = () => {
    const [orders, setOrders] = useState([]);
    const router = useRouter();
    useEffect(() => {
        getOrders().then(res => {
            setOrders(res.data?.data)
        })
    }, []);
    return <>
        <Container  maxW={'container.xl'} py={12} px={6} className='AdminOrders' >
        <Navbar />
            <Heading as='h3' size='lg'>
                Orders
            </Heading>
        <Table className='orders-table' variant='striped' colorScheme='white'>
            <Thead> 
                <Tr>
                    <Th className='name-header' >Name</Th>
                    <Th>Address</Th>
                    <Th>State</Th>
                    <Th>Country</Th>
                    <Th>Zip</Th>
                    <Th>Order Placed At</Th>
                    <Th>Price</Th>
                    <Th className='product-header' >Product</Th>
                </Tr>
            </Thead>
            <Tbody>
                {
                    orders.length && orders.map(order =>
                        <Tr
                            onClick={() => {
                                if (!order.attributes?.isDirect) {
                                    router.push(`/details/${order.attributes?.productId}?adminView=true&userId=${order.attributes?.user}`)
                                }
                            }}
                            style={{
                                cursor: !order.attributes?.isDirect ? 'pointer': 'default'
                            }}
                        >
                            <Td>{order.attributes?.name}</Td>
                            <Td>{order.attributes?.address + ' ' + (order?.address2 || ' ')}</Td>
                            <Td>{order.attributes?.state}</Td>
                            <Td>{order.attributes?.country}</Td>
                            <Td>{order.attributes?.zipCode}</Td>
                            <Td>{order.attributes?.createdAt}</Td>
                            <Td> <span className={order.attributes?.price ? 'price-cell' : ''} > {order.attributes?.price} </span></Td>
                            <Td>{order.attributes?.productName}</Td>
                        </Tr>
                    )
                }
            </Tbody>
        </Table>
        </Container>
    </>
}
export default Orders;