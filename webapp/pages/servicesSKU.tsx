import { NextPage } from "next";
import { Grid } from "@chakra-ui/react";
import React, {useContext, useEffect, useState} from "react";
import { useRouter } from "next/router";
import { products as productsSKU } from "../constants/skus";
// COMPONENTS
import ServicesCard from "components/services/Card";

// LAYOUT
import ServicesLayout from "layout/services";

// CONTEXT
import { CartContext } from "context/cart";

// TYPES
import PageWithLayoutType from "types/pageWithLayout";

// SERVICES
import {getProducts, getSKUProducts} from "services/api/store";
import ServicesLayoutSKU from "layout/servicesSKU";
import {useDomEvent} from "framer-motion";


const Services: NextPage = () => {
  const [products, setProducts] = useState([])
  const [productsSKU, setProductsSKU] = useState([]);

  const { services: cart } = useContext(CartContext)
  const router = useRouter();
  const category = router.query?.category || 'Development';

  useEffect(() => {
      getSKUProducts().then(res => {
          setProductsSKU(res.data.data.products);
      })
  }, [])

  useEffect(() => {
    setProducts(productsSKU.map(s => ({
      ...s,
      id: s.sku
    })).filter(pSKU => pSKU.category === category))
  }, [category, productsSKU]);

  return (
      <>
        <Grid mt={"2rem"} mx={{base: '2rem', md: '3rem'}} gridTemplateColumns={{base: 'repeat(2, 1fr)', md: 'repeat(2, 1fr)', lg: 'repeat(3, 1fr) '}} gridGap={'1rem'}>
          {
              products.length > 0 && products.map((service: any, index: number) => {
                    console.log({ service, cart }, cart.some(prod => `${prod.id}` === `${service?.id}`))
                    return <ServicesCard
                        key={index}
                        image={service?.attributes?.coverImage?.data?.attributes?.url || ''}
                        name={service?.attributes?.name || ''}
                        description={service?.attributes?.summary || ''}
                        isInCart={cart.some(prod => `${prod.id}` === `${service?.id}`)}
                        id={service.id}
                        {...service}
                        sku={service.sku}
                    />
                  }
              )
          }
        </Grid>
      </>
  );
};

(Services as PageWithLayoutType).layout = ServicesLayoutSKU;

export default Services;
