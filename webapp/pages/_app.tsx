import { ChakraProvider } from "@chakra-ui/react";
import Head from "next/head";
import React from "react";
import './admin/orders.scss'

// THEME
import NovatioTheme from "theme";

// CONTEXT
import { BreadcrumbsProvider } from "context/breadCrumbs";
import { CartProvider } from "context/cart";
import { ProcessProvider } from "context/process";

// TYPES
import { AppLayoutProps } from "types/_app";

// STYLES
import 'styles/global.module.scss';

function MyApp({ Component, pageProps }: AppLayoutProps) {
  const Layout = Component.layout || (({ children }) => <>{children}</>);

  return (
    <>
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin={"true"}
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Cabin:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap"
          rel="stylesheet"
        />
      </Head>

      <ChakraProvider theme={NovatioTheme}>
        <ProcessProvider>
          <CartProvider>
            <BreadcrumbsProvider>
              <Layout>
                <Component {...pageProps} />
              </Layout>
            </BreadcrumbsProvider>
          </CartProvider>
        </ProcessProvider>
      </ChakraProvider>
    </>
  );
}

export default MyApp;
