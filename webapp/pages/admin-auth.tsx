import {
    Flex,
    Box,
    FormControl,
    FormLabel,
    Input,
    Checkbox,
    Stack,
    Link,
    Button,
    Heading,
    Text,
    useColorModeValue, Image, AlertIcon, Alert,
} from '@chakra-ui/react';
import {useEffect, useState} from "react";
import { login } from "services/api/admin";
import {useRouter} from "next/router";
import {getRawProjectId} from "next/dist/telemetry/project-id";

export default function AdminAuth() {

    const [authMode, setAuthMode] = useState<string>('login');
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [serverError, setServerError] = useState<string>('');
    const router = useRouter();

    useEffect(() => {
        setServerError('')
    }, [email, password, authMode])


    const handleSubmit = async () => {
        try {
           login(email, password)
               .then(res => {
                   console.log(res.data)
                  localStorage.novatioAdminToken = res.data;
                   router.push('/admin/orders');
               });
        } catch (e) {
            // @ts-ignore
            setServerError(e.response?.data?.error?.message)
        }
    }
    return (
        <Flex
            minH={'100vh'}
            align={'center'}
            justify={'center'}
            bg={useColorModeValue('black.50', 'white.800')}>

            <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
                {/* eslint-disable-next-line @next/next/no-img-element */}
                <img
                    width={'100%'}
                    src={"https://i.ibb.co/DfkckKP/NOVATIO.png"}
                    alt={"Novatio logo"}
                />
                <Stack align={'center'}>
                    {/*// @ts-ignore*/}
                    <Heading fontSize={'4xl'}>Login to</Heading>
                    <Text fontSize={'lg'} color={'gray.600'}>
                        to access the admin panel
                    </Text>
                </Stack>
                <Box
                    rounded={'lg'}
                    bg={'#121212'}
                    boxShadow={'lg'}
                    p={8}>
                    <Stack spacing={4}>
                        <FormControl id="email">
                            <FormLabel>Email address</FormLabel>
                            <Input
                                type="email"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                            />
                        </FormControl>
                        <FormControl id="password">
                            <FormLabel>Password</FormLabel>
                            <Input
                                type="password"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                            />
                        </FormControl>
                        <Stack spacing={10}>
                            <Stack
                                direction={{ base: 'column', sm: 'row' }}
                                align={'start'}
                                justify={'space-between'}>
                                {/*<Link color={'blue.400'}>Forgot password?</Link>*/}
                            </Stack>
                            { serverError && <Stack
                                spacing={3}>
                                <Alert
                                    colorScheme={'brand'}
                                    bg={'black'}
                                    status='error'>
                                    <AlertIcon/>
                                    {serverError}
                                </Alert>
                            </Stack>
                                }
                            <Button
                                colorScheme={'brand'}
                                onClick={handleSubmit}
                                >
                                Login
                            </Button>
                        </Stack>
                    </Stack>
                </Box>
            </Stack>
        </Flex>
    );
}