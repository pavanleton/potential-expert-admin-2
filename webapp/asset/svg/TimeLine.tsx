import * as React from "react";

const Timeline = (props: any) => (
  <svg
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <circle cx={48.5} cy={48.5} r={48} stroke="#25A146" />
    <circle cx={48.5} cy={48.5} r={31.174} stroke="#25A146" />
  </svg>
);

export default Timeline;
