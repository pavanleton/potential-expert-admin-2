  'use strict';
  const { getUserFromRequest, standardizeDBResult, calculateCost} = require("../../../utils");
  const stripe = require('stripe')(process.env.STRIPE_SECRET);
  const { sign } = require('jsonwebtoken');
  const { skuProducts, categories}= require('../../../skus');
  /**
   * A set of functions called "actions" for `custom-orders`
   */
  const YOUR_DOMAIN = process.env.CLIENT_DOMAIN;
  const createOrders = async (_orders, strapi) => {
    const o = [];
    for (const ord of _orders) {
      const o2 = await strapi.entityService.create('api::order.order', {
        data: ord,
      });
      o.push(o2)
    }
    return o;
  }

  module.exports = ({ strapi }) => ({
    async getUserOrders (ctx) {
      const { id, role } = getUserFromRequest(ctx.request);
      const uid = role === 'admin'?  ctx.request.body.userId : id;
      const data = await strapi.db.query('api::order.order').findMany({
        where: {
          ...ctx.query?.filter,
          user: uid,
        },
        sort: { createdAt: 'DESC' },
      });
       return { data: standardizeDBResult(data) }
    },

    async placeOrder (ctx) {
      const { id } = getUserFromRequest(ctx.request);
      const { products, orderDetails } =  ctx.request.body;


      // fetch the products from database
      const productEntries = await strapi.db.query('api::product.product').findMany({
        where: {
          id: products.map(pr => pr.id),
        },
      });

      const getC = (pr) => {
        const p =  products.find(p => p.id === pr.id)
        return calculateCost(Number(p.transactionsPerMonth), Number(p.humanMinutesPerTransaction))
      }

      const newOrders = productEntries.map(pr => ({
        ...orderDetails,
        productId: String(pr.id),
        productName: pr.name,
        user: id,
        price: String(Math.floor(getC(pr))),
      }));

      const orders = await createOrders(newOrders, strapi);

      const stripeLineItems = products.map(prd => ({
        price_data: {
          currency: 'usd',
          product_data: {
            name: prd.name,
            description: `SKU: ${prd.sku}. Transactions per month: ${prd.transactionsPerMonth} & human minutes: ${prd.humanMinutesPerTransaction}`
          },
          unit_amount_decimal: Math.floor(calculateCost(Number(prd.transactionsPerMonth), Number(prd.humanMinutesPerTransaction))) * 100,
          tax_behavior: 'exclusive',
        },
        quantity: 1,
      }));

      const session = await stripe.checkout.sessions.create({
        line_items: stripeLineItems,
        mode: 'payment',
        success_url: `${YOUR_DOMAIN}/verify-payment?result=success&orders=${orders.map(o => o.id).join(',')}`,
        cancel_url: `${YOUR_DOMAIN}/verify-payment?result=failure&orders=${orders.map(o => o.id).join(',')}}`,
        automatic_tax: {enabled: true},
      });

      await strapi.db.query('api::order.order').updateMany({
        where: {
          id: orders.map(o => o.id),
        },
        data: {
          stripeSessionId: session.id,
        },
      });

      return { data: {sessionUrl: session.url } };
    },

    async placeDirectOrders (ctx) {
      const { id } = getUserFromRequest(ctx)
      const { SKUs, orderDetails } = ctx.request.body;
      const products = skuProducts.filter(pr => SKUs.includes(pr.sku));
      const newOrders = products.map(pr => ({
        ...orderDetails,
        productId: String(pr.sku),
        productName: pr.name,
        user: id,
        price: String(parseInt(pr.cost.replace( /^\D+/g, '').replace(',',''))),
        isDirect: true,
      }));

      const orders = await createOrders(newOrders, strapi);

      const stripeLineItems = products.map(prd => ({
        price_data: {
          currency: 'usd',
          product_data: {
            name: prd.name,
            description: `SKU: ${prd.sku}. ${prd.description}`
          },
          unit_amount_decimal: parseInt(prd.cost.replace( /^\D+/g, '').replace(',','')) * 100,
          tax_behavior: 'exclusive',
        },
        quantity: 1,
      }));

      const session = await stripe.checkout.sessions.create({
        line_items: stripeLineItems,
        mode: 'payment',
        success_url: `${YOUR_DOMAIN}/verify-payment?result=success&orders=${orders.map(o => o.id).join(',')}`,
        cancel_url: `${YOUR_DOMAIN}/verify-payment?result=failure&orders=${orders.map(o => o.id).join(',')}}`,
        automatic_tax: {enabled: true},
      });

      await strapi.db.query('api::order.order').updateMany({
        where: {
          id: orders.map(o => o.id),
        },
        data: {
          stripeSessionId: session.id,
        },
      });

      return { data: {sessionUrl: session.url } };
    },

    async updateOrderPaymentStatus(ctx) {
      const userDecoded = getUserFromRequest(ctx.request);
      const userId = userDecoded.id;
      const { orders } = ctx.request.body;

      const order = await strapi.db.query('api::order.order').findOne({
        where: {
          user: userId,
          id: orders[0],
        }
      });
      const stripeSession = await stripe.checkout.sessions.retrieve(order.stripeSessionId);
      const status = stripeSession.payment_status === 'paid' ? 'payment_complete': 'payment_pending';
      await strapi.db.query('api::order.order').updateMany({
        where: {
          id: orders,
        },
        data: {
          status,
        },
      });
        return status;
    },

    getSKUProducts(ctx) {
      return {
        data: {
          products: skuProducts,
          categories
        }
      }
    }
  });

