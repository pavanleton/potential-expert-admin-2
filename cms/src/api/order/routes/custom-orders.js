
module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/orders/me',
      handler: 'custom-orders.getUserOrders',
        config: {
          policies: ['global::is-user'],
          auth: false
        }
    },
    {
      method: 'POST',
      path: '/orders/place',
      handler: 'custom-orders.placeOrder',
      config: {
        policies: ['global::is-user'],
        auth: false
      }
    },
    {
      method: 'POST',
      path: '/orders/place/direct',
      handler: 'custom-orders.placeDirectOrders',
      config: {
        policies: ['global::is-user'],
        auth: false
      }
    },
    {
      method: 'PATCH',
      path: '/orders/payment-status',
      handler: 'custom-orders.updateOrderPaymentStatus',
      config: {
        policies: ['global::is-user'],
        auth: false
      }
    },
    {
      method: 'GET',
      path: '/products/sku',
      handler: 'custom-orders.getSKUProducts',
      config: {
        auth: false
      }
    },
  ]
}
