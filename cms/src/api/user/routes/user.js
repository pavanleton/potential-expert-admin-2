module.exports = {
  routes: [
    {
     method: 'POST',
     path: '/user/admin/login',
     handler: 'user.adminLogin',
     config: {
       auth: false,
     },
    },
  ],
};
