'use strict';
const { sign } = require('jsonwebtoken');
/**
 * A set of functions called "actions" for `user`
 */

module.exports = ({ strapi }) => ({
  async adminLogin (ctx) {
    const { email, password } = ctx.request.body;
    const adminCreds = process.env.ADMIN_CREDS.split(',')
    if (email === adminCreds[0] && password === adminCreds[1]) {
      return sign({
        role: 'admin',
        id: email,
      }, process.env.JWT_SECRET, {
        expiresIn: '7d',
      })
    }
  }
});
