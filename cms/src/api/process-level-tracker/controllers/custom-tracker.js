'use strict';
const { getUserFromRequest, standardizeDBResult} = require("../../../utils");
/**
 * A set of functions called "actions" for `custom-orders`
 */
module.exports = ({ strapi }) => ({
  async getUserTracker (ctx) {
    const { id, role } = getUserFromRequest(ctx.request);
    const { processLevel, userId } = ctx.query;
    const uid = role === 'admin'? userId : id;
    const entry = await strapi.db.query('api::process-level-tracker.process-level-tracker').findOne({
      where: {
        processLevel,
        user: uid,
      },
    });
    if (!entry) {
      const { subProcesses } = await strapi.entityService.findOne('api::process-level.process-level',
        processLevel, {
        populate: { subProcesses: true }
      });
      const initialTrackingInfo = subProcesses.map(sp => ({
        ...sp,
        isCompleted: false,
      }));
      const newEntry = await strapi.entityService.create('api::process-level-tracker.process-level-tracker', {
        data: {
          trackingInfo: initialTrackingInfo,
          user: uid,
          processLevel,
        },
      });
      return { data: newEntry };
    }
    return { data: entry }
  },

  async updateUserTracker (ctx) {
    const { id, role } = getUserFromRequest(ctx.request);
    const uid = role === 'admin'? ctx.request.body.userId : id;
    const { trackingInfo, processLevelId } = ctx.request.body.data;
    // TODO: Add JSON Validation for trackingInfo
    const entry = await strapi.db.query('api::process-level-tracker.process-level-tracker').update({
      where: {
        user: uid,
        processLevel : processLevelId,
      },
      data: {
        trackingInfo,
      },
    });
    return { data: entry };
  }
});

