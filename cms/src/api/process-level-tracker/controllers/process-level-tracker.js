'use strict';

/**
 *  process-level-tracker controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::process-level-tracker.process-level-tracker');
