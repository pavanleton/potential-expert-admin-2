'use strict';

/**
 * process-level-tracker router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::process-level-tracker.process-level-tracker');
