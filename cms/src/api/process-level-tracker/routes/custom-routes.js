
module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/process-level-trackers/me',
      handler: 'custom-tracker.getUserTracker',
      config: {
        policies: ['global::is-user'],
        auth: false
      }
    },
    {
      method: 'PATCH',
      path: '/process-level-trackers/me',
      handler: 'custom-tracker.updateUserTracker',
      config: {
        policies: ['global::is-user'],
        auth: false
      }
    },
  ]
}
