'use strict';

/**
 * process-level-tracker service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::process-level-tracker.process-level-tracker');
