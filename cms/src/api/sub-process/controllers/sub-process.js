'use strict';

/**
 *  sub-process controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::sub-process.sub-process');
