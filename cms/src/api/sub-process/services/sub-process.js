'use strict';

/**
 * sub-process service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::sub-process.sub-process');
