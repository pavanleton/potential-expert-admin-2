'use strict';

/**
 * sub-process router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::sub-process.sub-process');
