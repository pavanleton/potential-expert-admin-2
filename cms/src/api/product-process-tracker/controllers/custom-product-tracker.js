'use strict';
const { getUserFromRequest, standardizeDBResult} = require("../../../utils");
/**
 * A set of functions called "actions" for `custom-orders`
 */
module.exports = ({ strapi }) => ({
  async getUserTracker (ctx) {
    const { id, role } = getUserFromRequest(ctx.request);
    const { product, userId } = ctx.query;
    const uid = role === 'admin'? userId : id;
    console.log({ product, role, uid })
    const entry = await strapi.db.query('api::product-process-tracker.product-process-tracker').findOne({
      where: {
        product,
        user: uid,
      },
    });
    if (!entry) {
      const { processLevels } = await strapi.entityService.findOne('api::product.product',
        product, {
          populate: { processLevels: true }
        });
      const initialTrackingInfo = processLevels.map(sp => ({
        ...sp,
        isCompleted: false,
      }));
      const newEntry = await strapi.entityService.create('api::product-process-tracker.product-process-tracker', {
        data: {
          trackingInfo: initialTrackingInfo,
          user: uid,
          product,
        },
      });
      return { data: newEntry };
    }
    return { data: entry }
  },

  async updateUserTracker (ctx) {
    const { id, role } = getUserFromRequest(ctx.request);
    const uid = role === 'admin'? ctx.request.body.userId : id;
    const { trackingInfo, productId } = ctx.request.body.data;
    // TODO: Add JSON Validation for trackingInfo
    const entry = await strapi.db.query('api::product-process-tracker.product-process-tracker').update({
      where: {
        user: uid,
        product: productId,
      },
      data: {
        trackingInfo,
      },
    });
    return { data: entry };
  }
});

