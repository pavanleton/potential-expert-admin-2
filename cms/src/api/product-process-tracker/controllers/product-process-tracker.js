'use strict';

/**
 *  product-process-tracker controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::product-process-tracker.product-process-tracker');
