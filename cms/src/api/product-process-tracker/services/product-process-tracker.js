'use strict';

/**
 * product-process-tracker service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::product-process-tracker.product-process-tracker');
