'use strict';

/**
 * product-process-tracker router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::product-process-tracker.product-process-tracker');
