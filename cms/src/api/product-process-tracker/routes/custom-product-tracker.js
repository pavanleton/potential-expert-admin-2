module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/product-process-trackers/me',
      handler: 'custom-product-tracker.getUserTracker',
      config: {
        policies: ['global::is-user'],
        auth: false
      }
    },
    {
      method: 'PATCH',
      path: '/product-process-trackers/me',
      handler: 'custom-product-tracker.updateUserTracker',
      config: {
        policies: ['global::is-user'],
        auth: false
      }
    },
  ]
}
