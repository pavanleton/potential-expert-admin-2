'use strict';

/**
 * super-type router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::super-type.super-type');
