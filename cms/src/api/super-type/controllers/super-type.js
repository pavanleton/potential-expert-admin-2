'use strict';

/**
 *  super-type controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::super-type.super-type');
