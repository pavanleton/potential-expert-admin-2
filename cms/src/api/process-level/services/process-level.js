'use strict';

/**
 * process-level service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::process-level.process-level');
