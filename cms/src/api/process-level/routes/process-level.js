'use strict';

/**
 * process-level router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::process-level.process-level');
