'use strict';

/**
 *  process-level controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::process-level.process-level');
