module.exports.skuProducts = [
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Development",
    "sku": "Automation-AFP-001",
    "name": "Off Shore Agile Flex Pod",
    "description": "Offshore Flexible development Pod.",
    "cost": "US$ 24,000.00",
    "unitOfMeasure": "per month"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Development",
    "sku": "Automation-AFP-002",
    "name": "On Shore Agile Flex Pod",
    "description": "Onshore Flexible Development Pod",
    "cost": "US$ 41,500.00",
    "unitOfMeasure": "per month"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Development",
    "sku": "Automation-AFP-003",
    "name": "Hybrid Agile Flex Pod",
    "description": "Flexible development pod,  with a mix of Onshore and Offshore Resources",
    "cost": "US$ 34,500.00",
    "unitOfMeasure": "per month"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Development",
    "sku": "Automation-Formsl-001",
    "name": "Paper Forms  Processing - Tier 1",
    "description": "Converting Paper Forms to Digital. Includes up to 4 forms with 15 pages total.",
    "cost": "US$ 20,000.00",
    "unitOfMeasure": "per bundle"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Development",
    "sku": "Automation-Formsl-002",
    "name": "Paper Forms Processing - Tier 2",
    "description": "Converting Paper Forms to Digital. Includes up to 10 forms with 45 pages total.",
    "cost": "US$ 55,000.00",
    "unitOfMeasure": "per bundle"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Development",
    "sku": "Automation-Froms-003",
    "name": "Paper Forms Processing - Tier 3",
    "description": "Converting Paper Forms to Digital. Includes up to 25 forms with 100 pages total.",
    "cost": "US$ 88,000.00",
    "unitOfMeasure": "per bundle"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Managed Services",
    "sku": "Automation-MSP-008",
    "name": "Enterprise- HyperAutomation Journey - Tier 1",
    "description": "Organization Wide Continual Innovation Service Model to quickly scale your automation program from the main growth levers:   for Program Creation,  Process Discovery. Automation Development, Continual Improvement and Support.",
    "cost": "US$ 8,75,000.00",
    "unitOfMeasure": "per year"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Managed Services",
    "sku": "Automation-MSP-009",
    "name": "Enterprise- HyperAutomation Journey - Tier 1",
    "description": "Organization Wide Continual Innovation Service Mode version 2.0 with a focus on cogntive technology to grow and scale your automation program from the four growth levers.     l to quickly scale your automation program from the main growth levers:   for Mature Program Creation,  Automated Process Discovery and Mining,, Cognitive Automation Development, Continual Improvement and Support, Business user (Citizen) training and organization wide enablement.",
    "cost": "US$ 26,25,000.00",
    "unitOfMeasure": "per year"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Development",
    "sku": "Automation-SA-001",
    "name": "Process Based Automation Single Process -- Level 1",
    "description": "Automation of a simple complexity process including discovery, design, development, deployment and hypercare.",
    "cost": "US$ 35,000.00",
    "unitOfMeasure": "per month"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Development",
    "sku": "Automation-SA-002",
    "name": "Process Based Automation Single Process -- Level 2",
    "description": "Automation of a medium complexity process including discovery, design, development, deployment and hypercare.",
    "cost": "US$ 66,000.00",
    "unitOfMeasure": "per month"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Development",
    "sku": "Automation-SA-003",
    "name": "Process Based Automation Single Process -- Level 3",
    "description": "Automation of a high complexity process including discovery, design, development, deployment and hypercare.",
    "cost": "US$ 93,000.00",
    "unitOfMeasure": "per month"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Development",
    "sku": "Automation-POC-001",
    "name": "Automation of POC Process",
    "description": "Proof of Concept with Level 1 Discovery and Development.",
    "cost": "US$ 9,000.00",
    "unitOfMeasure": "per poc"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Process Discovery",
    "sku": "Automation-PLT-001",
    "name": "Automation of Pilot Process",
    "description": "Automation of Pilot process from Ideation, Business Case Modelling, Design, Development, Deployement to Production.",
    "cost": "US$ 45,000.00",
    "unitOfMeasure": "per pilot"
  },
  {
    "CATEGORY NAME": "",
    "category": "Process Discovery",
    "sku": "Automation-DISC-O01",
    "name": "Process Discovery Tier 1",
    "description": "Process Discovery, Pipeline Creation, ROI Calculation., and Business Case Modeling for up to 8 Processes",
    "cost": "US$ 18,000.00",
    "unitOfMeasure": "per month"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Process Discovery",
    "sku": "Automation-DISC-002",
    "name": "Process Discovery Tier 2",
    "description": "Process Discovery, Pipeline Creation, ROI Calculation., and Business Case Modeling for up to 15 processes",
    "cost": "US$ 32,000.00",
    "unitOfMeasure": "per month"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Program Creation",
    "sku": "AOM-Ready-001",
    "name": "Automation Readiness Program",
    "description": "This program helps evaluate and educate your organization on Intelligent Automation.\nWe will evaluate your IT staff, IT systems, and Businesss Teams,and Automation Culture using:  awareness & nablement workshops, technology suitability assessment, use case Ideation workshops.",
    "cost": "US$ 18,500.00",
    "unitOfMeasure": "per instance"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Program Creation",
    "sku": "AOM-MSP-001",
    "name": "GEN - Automation Operating Model - Phase 1",
    "description": "Establishment of Steering Committees, Auditing, compliance, controls, COE, Governance, Policy Generation, Standard Process Design, Citizen Deployment Development",
    "cost": "US$ 2,00,000.00",
    "unitOfMeasure": "per year"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Managed Services",
    "sku": "AOM-MSP-002",
    "name": "GEN - Automation Operating Model - Phase 2",
    "description": "Maintenance and Continuous improvement of Steering Committees, Auditing, compliance, controls, COE, Governance, Policy Generation, Standard Process Design, Citizen Deployment Development",
    "cost": "US$ 80,000.00",
    "unitOfMeasure": "per year"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Program Creation",
    "sku": "Automation-COE-001",
    "name": "GEN - COE Buildout",
    "description": "Build internal Center of Excellence",
    "cost": "US$ 24,000.00",
    "unitOfMeasure": "per month"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Program Creation",
    "sku": "Automation-Ready-002",
    "name": "GEN - Automation JumpStart",
    "description": "Automation Readiness Program, Governance, and automation of two simple processes.",
    "cost": "US$ 54,000.00",
    "unitOfMeasure": "one time"
  },
  {
    "CATEGORY NAME": "",
    "category": "Infrastructure",
    "sku": "INF-Platform-001",
    "name": "Infrastructure Setup Level 1",
    "description": "Installation of Core  RPA Platform",
    "cost": "US$ 12,000.00",
    "unitOfMeasure": "One Time"
  },
  {
    "CATEGORY NAME": "",
    "category": "Infrastructure",
    "sku": "INF-Platform-002",
    "name": "Infrastructure Setup Level 2",
    "description": "Installation of Core  RPA Platform with Document Understanding, Process Mining, and technology extenders",
    "cost": "US$ 14,500.00",
    "unitOfMeasure": "One Time"
  },
  {
    "CATEGORY NAME": "",
    "category": "Infrastructure",
    "sku": "INF-Platform-003",
    "name": "Infrastructure Setup Level 3",
    "description": "Installation and testing of Core RPA Platform, RPA Enablers, and OEM tools like advanced analytics, chatbots, AI, IVR, NLP",
    "cost": "US$ 19,500.00",
    "unitOfMeasure": "One Time"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Support and Maintenance",
    "sku": "Automation - MSP - 004",
    "name": "Offshore Support Basic",
    "description": "7x12 support for up to 10 processes",
    "cost": "US$ 9,500.00",
    "unitOfMeasure": "per month"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Support and Maintenance",
    "sku": "Automation-MSP-005",
    "name": "Onshore Support Basic",
    "description": "7x12 support for up to 10 processes",
    "cost": "US$ 15,000.00",
    "unitOfMeasure": "per month"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Support and Maintenance",
    "sku": "Automation-MSP-006",
    "name": "Block Hour Support -- Tier 1",
    "description": "2500 hours for services paid upfront.",
    "cost": "US$ 2,37,500.00",
    "unitOfMeasure": "per bundle"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Support and Maintenance",
    "sku": "Automation-MSP-007",
    "name": "Managed Robotic Program -  Operations Center",
    "description": "10 processes - 24/7 Managed Support  - Monitoring, Platform Support, Continuous Optimization",
    "cost": "US$ 39,000.00",
    "unitOfMeasure": "per month"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Training",
    "sku": "Training-AN-001",
    "name": "RPA Foundation Training",
    "description": "General RPA Training.",
    "cost": "US$ 7,500.00",
    "unitOfMeasure": "per week"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Training",
    "sku": "Training-AN-002",
    "name": "RPA Technical Training Bootcamp",
    "description": "Introduction boot camp with hands on training to build processes.",
    "cost": "US$ 24,000.00",
    "unitOfMeasure": "3 weeks"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Training",
    "sku": "Training-AN-003",
    "name": "Training - Boot Camp Emerging Technologies",
    "description": "Boot camp with hands on training to build processes. Document Understanding, Process Mining, HyperAutomation, Chatbot, No Code/Low Code, IVR, NLP.",
    "cost": "US$ 24,000.00",
    "unitOfMeasure": "2 weeks"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Training",
    "sku": "Training-AN-004",
    "name": "Dedicated On-Going Training",
    "description": "Training that is Customized to cover all departs of organization, and their unique needs.",
    "cost": "US$ 1,50,000.00",
    "unitOfMeasure": "per year"
  },
  {
    "CATEGORY NAME": "Govt Implementation Services",
    "category": "Training",
    "sku": "Training-AN-005",
    "name": "SBot-A-Thon",
    "description": "Instructors work with departments to train employees to build the bots and process. Enables organization wide buy-in to increase automation potential by 70%",
    "cost": "US$ 20,000.00",
    "unitOfMeasure": "per month"
  }
];

module.exports.categories = ['Development','Managed Services', 'Process Discovery', 'Program Creation', 'Infrastructure', 'Support and Maintenance', 'Training'];
