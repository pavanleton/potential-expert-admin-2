'use strict';

/**
 * `is-admin` policy.
 */

const {verify} = require("jsonwebtoken");
module.exports = (policyContext, config ) => {

  const token = policyContext.request.header.authorization.replace('Bearer ', '');
  const decoded = verify(token, process.env.JWT_SECRET);

  if (decoded.role === 'admin') {
    return true;
  }
  return false;
};
