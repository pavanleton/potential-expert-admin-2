'use strict';

/**
 * `is-user` policy.
 */

const {verify} = require("jsonwebtoken");
module.exports = (policyContext, config ) => {
    // Add your own logic here.
  const token = policyContext.request.header.authorization.replace('Bearer ', '');
  const decoded = verify(token, process.env.JWT_SECRET);
  if (decoded.id) {
      return true;
    }
    return false;
};
