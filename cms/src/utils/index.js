const {verify} = require("jsonwebtoken");
const getUserFromRequest = (req) => {
  const token = req.header.authorization.replace('Bearer ', '');
  return verify(token, process.env.JWT_SECRET);
}

const standardizeDBResult = (data) => {
  return data.map(d => ({
    id: d.id,
    attributes: {
      ...d,
    }
  }))
}
const calculateCost = (transactionsPerMonth, humanMinutesPerTransaction, hoursPerRobot = 320) => {
  const costPerRobot = 10000;
  const minutesPerMonth = transactionsPerMonth * humanMinutesPerTransaction;
  const hoursPerMonth = minutesPerMonth / 60;
  const hoursPerYear = hoursPerMonth * (12);
  const robotsPerYear = hoursPerYear / (hoursPerRobot * 12);
  return (robotsPerYear * costPerRobot);
}

module.exports = {
  getUserFromRequest,
  standardizeDBResult,
  calculateCost,
}
